﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Globalization;

namespace Kolen
{
    public static class cExtensions
    {

        public static bool IsNumeric(this object Text)
        {
            try
            {
                int number = Convert.ToInt32(Text);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsEmail(this string Text)
        {
            try
            {
                MailAddress m = new MailAddress(Text);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public static string ToEN(this string Text)
        {
            Text = Regex.Replace(Text, "ş", "s");
            Text = Regex.Replace(Text, "ı", "i");
            Text = Regex.Replace(Text, "ö", "o");
            Text = Regex.Replace(Text, "ü", "u");
            Text = Regex.Replace(Text, "ç", "c");
            Text = Regex.Replace(Text, "ğ", "g");
            Text = Regex.Replace(Text, "Ş", "S");
            Text = Regex.Replace(Text, "İ", "I");
            Text = Regex.Replace(Text, "Ö", "O");
            Text = Regex.Replace(Text, "Ü", "U");
            Text = Regex.Replace(Text, "Ç", "C");
            Text = Regex.Replace(Text, "Ğ", "G");

            return Text;
        }

        public static string ToURL(this string Text)
        {
            string title = Text;
            title = title.Trim();
            title = title.Trim('-');
            title = title.ToLower();

            char[] chars = @"$%#@!*?;:~`’+=()[]{}|\'<>,/^&™"".".ToCharArray();

            for (int i = 0; i < chars.Length; i++)
            {
                string strChar = chars.GetValue(i).ToString();
                if (title.Contains(strChar))
                    title = title.Replace(strChar, string.Empty);
            }

            title = title.Replace(" ", "-");
            title = title.Replace("--", "-");
            title = title.Replace("---", "-");
            title = title.Replace("----", "-");
            title = title.Replace("-----", "-");
            title = title.Replace("----", "-");
            title = title.Replace("---", "-");
            title = title.Replace("--", "-");
            title = title.Replace("ü", "u");
            title = title.Replace("ğ", "g");
            title = title.Replace("ş", "s");
            title = title.Replace("ö", "o");
            title = title.Replace("ç", "c");
            title = title.Replace("ı", "i");
            title = title.Replace("İ","I");
            title = title.Replace("Ü", "U");
            title = title.Replace("Ö", "O");

            title = title.Trim();
            title = title.Trim('-');
            

            return title;
        }

        public static int ToReverse(this int CurrentValue)
        {
            if (CurrentValue == 1)
                return 0;
            else
                return 1;
        }

        public static bool ToBool(this object Value)
        {
            if (Convert.ToInt32(Value) == 1)
                return true;
            else
                return false;
        }

        public static string ToMD5(this string ClearText)
        {
            byte[] ByteData = Encoding.ASCII.GetBytes(ClearText);
            MD5 oMd5 = MD5.Create();
            byte[] HashData = oMd5.ComputeHash(ByteData);
            StringBuilder oSb = new StringBuilder();

            for (int x = 0; x < HashData.Length; x++)
            {
                oSb.Append(HashData[x].ToString("x2"));
            }

            return oSb.ToString();
        }

  

        public static bool IsTextBoxEmptyOrNull(this string Text)
        {

            if (String.IsNullOrEmpty(Text))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static bool AreAllTextboxIsEmptyOrNull(this Panel ID)
        {

            foreach (Control item in ID.Controls)
            {
                if (item is TextBox)
                {
                    TextBox txt = (TextBox)item;
                    if (String.IsNullOrEmpty(txt.Text))
                    {

                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }

            }
            return false;
        }

        public static bool ZipCode(this string Text)
        {
            bool zipdogrumu = Regex.IsMatch(Text, "[a-zA-Z0-9]$");

            if (zipdogrumu)  //"^[0-9A-Za-z ]+$" 
            {
                if (Text.Length < 30)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public static bool OnlyLetters(this string Text)
        {
            bool uyuyomu = Regex.IsMatch(Text, "[^a-zA-ZöÖçÇİışŞÜü\\s]$");
            if (uyuyomu == false)
            {
                if (Text.IsTextBoxEmptyOrNull())
                {
                    return false;
                }
                else
                {
                    if (Text.Length < 35)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }

            }
            else
            {
                return false;
            }
        }

        public static bool TCKimlik(this string Text)
        {
            bool uyuyomu = Regex.IsMatch(Text, "^[1-9]{1}[0-9]{10}$");//"^[0-9A-Za-z ]+$" 

            if (uyuyomu)
            {
                if (Text.Length > 11)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }

        public static bool IsDogum(this object Text)
        {
            try
            {

                DateTime dt = Convert.ToDateTime(Text);

                return true;
            }
            catch (Exception)
            {

                return false;
            }

        }

        public static bool IsPhone(this string Text)
        {
            bool sabit = Regex.IsMatch(Text, "[+][9]{1}[0]{1}[0-9]{10}$");
            bool cep = Regex.IsMatch(Text, "[+][9]{1}[0]{1}[5]{1}[0-9]{9}$");

            if (sabit || cep)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static ArrayList GetSelectedItems(this Repeater rpt, string chkBoxId)
        {
            var selectedValues = new ArrayList();
            for (int i = 0; i < rpt.Items.Count; i++)
            {
                var chkBox = rpt.Items[i].FindControl(chkBoxId) as HtmlInputCheckBox;

                if (chkBox != null && chkBox.Checked)
                {

                    //string[] degerlen = chkBox.Attributes["class"].Split(' ');
                    selectedValues.Add(chkBox.Value);
                }

            }
            return selectedValues;
        }

        public static ArrayList GetSectionItems(this Repeater rpt, string chkBoxId)
        {
            var selectedValues = new ArrayList();
            for (int i = 0; i < rpt.Items.Count; i++)
            {
                var chkBox = rpt.Items[i].FindControl(chkBoxId) as HtmlInputCheckBox;

                if (chkBox != null && chkBox.Checked)
                {

                    //string[] degerlen = chkBox.Attributes["class"].Split(' ');
                    selectedValues.Add(chkBox.Value);
                }

            }
            return selectedValues;
        }

        public static bool fileHasFile(this FileUpload id)
        {

            if (id.HasFile)
            {
                return true;
            }
            else
            {
                return false;
            }


        }

        public static bool veriBagla(this Repeater rpt, IEnumerable veri)
        {
            var col = veri as ICollection;
            if (veri != null && col.Count > 0)
            {
                rpt.DataSource = veri;
                rpt.DataBind();
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void BindVeri(this Repeater rpt, IEnumerable veri)
        {
            var col = veri as ICollection;
            if (veri != null && col.Count > 0)
            {
                rpt.DataSource = veri;
                rpt.DataBind();
            }
        }

    }
}