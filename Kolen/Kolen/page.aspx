﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Kolen.Master" AutoEventWireup="true" CodeBehind="page.aspx.cs" Inherits="Kolen.page" %>
<%@ Register TagPrefix="leftMenu" TagName="leftMenu" Src="~/leftMenu.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row">
            <leftMenu:leftMenu runat="Server" id="leftmenuKurumsal"></leftMenu:leftMenu>
            <div class="span9">
                <div class="page-baslik">
                    <asp:Literal ID="ltrBaslik" runat="server" />
                </div>
                <div class="page-baslik mt10 page-icerik">
                     <asp:Literal ID="ltrIcerik" runat="server" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
