﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen
{
    public partial class iletisim : cBase
    {
        public string message;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected string uyarilar(string uyari, string durum) 
        {
            string sonuc = "<div class='alert alert-" + durum + "'>" +uyari +"</div>";
            return sonuc;
        }

        protected void btnGonder_Click(object sender, EventArgs e)
        {
            if (txtAdSoyad.Text.IsTextBoxEmptyOrNull())
            {
                lblUyari.Text=uyarilar("Ad Soyad alanı boş geçilemez.","danger");
         
            }
            else
            {
                if (!txtEPosta.Text.IsEmail())
                {
                    lblUyari.Text = uyarilar("E-Mail formatı doğru değil.", "danger");

                }
                else
                {
                    if (txtMesaj.Text.IsTextBoxEmptyOrNull())
                    {
                        lblUyari.Text = uyarilar("Mesaj alanı boş geçilemez.", "danger");

                    }
                    else
                    {
                        lblUyari.Text = "";
                        string konu = txtKonu.Text.IsTextBoxEmptyOrNull() ? "Konu Girilmedi" : txtKonu.Text;
                        bool basarili = false;
                        string uyariMetni = "";

                        string adSoyad =  txtAdSoyad.Text;

                        string telefon = txtTel.Text.IsTextBoxEmptyOrNull() ? "Telefon girilmedi." : txtTel.Text;
                        string eMail =  txtEPosta.Text;
                        string mesaj =  txtMesaj.Text;



                        string mail_msg = string.Empty;
                        mail_msg += "<table style='border:solid 1px #808080; color:#000000; background-color:#e2e2e2;'>";
                        mail_msg += "<tr><td colspan='2'><h3>Mail Bilgileri</h3></td></tr>";

                        mail_msg += "<tr><td>" + "Ad Soyad: " + "</td><td>" + adSoyad + "</td></tr>";
                        mail_msg += "<tr><td>" + "Konu: " + "</td><td>" + konu + "</td></tr>";
                        mail_msg += "<tr><td>" + "E-Mail: " + "</td><td>" + eMail + "</td></tr>";
                        mail_msg += "<tr><td>" + "IP: " + "</td><td>" + Request.UserHostAddress + "</td></tr>";
                        mail_msg += "<tr><td>" + "Tarih: " + "</td><td>" + DateTime.Now + "</td></tr>";

                        mail_msg += "<tr><td>" + "Mesaj : " + "</td><td>" + mesaj + "</td></tr>";
                        mail_msg += "</table>";


                        try
                        {
                            MailMessage mailMsg = new MailMessage();
                            mailMsg.To.Add("silo00x@gmail.com");
                            eMail = eMail.IsEmail() ? eMail : "silo00x@gmail.com";
                            MailAddress mailAddress = new MailAddress(eMail);
                            mailMsg.From = mailAddress;
                            mailMsg.Subject = konu;
                            mailMsg.Body = mail_msg;
                            mailMsg.IsBodyHtml = true;

                            SmtpClient smtpClient = new SmtpClient("mail.playback.com.tr", 587);
                            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("mesaj@playback.com.tr", "msg123");
                            smtpClient.Credentials = credentials;
                            smtpClient.Send(mailMsg);
                            uyariMetni = "Mesajınız Gönderilmiştir.";
                            basarili = true;
                        }
                        catch
                        {
                            uyariMetni = "Mail Gönderilirken hata oluştu.";

                        }



                        if (basarili)
                        {
                            lblUyari.Text = "<div class='alert alert-success'>" + uyariMetni + "</div>";
                            Response.Redirect("uyari.aspx?syf=iletisim&durum=basarili");
                        }
                        else
                        {
                            lblUyari.Text = "<div class='alert alert-error'>" + uyariMetni + "</div>";
                            Response.Redirect("uyari.aspx?syf=iletisim&durum=hata");
                        }




                    }
                }
            }



        }
    }
}