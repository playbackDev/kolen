﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Kolen.Master" AutoEventWireup="true" CodeBehind="faq.aspx.cs" Inherits="Kolen.faq" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script>
            $(document).ready(function () {

                $('.faqs dd').hide();
                $('.faqs dt').hover(function () { $(this).addClass('hover') }, function () { $(this).removeClass('hover') }).click(function () {
                    $('.faqs dd').hide();
                    $(this).next().slideToggle('normal');
                });
            });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="container">
        <div class="row">
            <div class="span12">
                <div class="page-baslik">
                    BİLGİLER
                </div>
                <div class="page-baslik mt10 page-icerik">
                    <dl class="faqs">
                            <asp:Repeater runat="server" ID="rptSorular">
                                <ItemTemplate>
                                    <dt><%#Eval("baslik")%></dt>
                                    <dd><%#Eval("detay")%></dd>
                                </ItemTemplate>
                            </asp:Repeater>
                        </dl>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
