﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen
{
    public partial class haber : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Request.QueryString["id"].IsTextBoxEmptyOrNull()&&Request.QueryString["id"].IsNumeric())
            {
                int id =Convert.ToInt16(Request.QueryString["id"]);
                var haber=db.kolen_news.Where(s=>s.id==id).FirstOrDefault();
                ltrBaslik.Text=haber.baslik;
                ltrIcerik.Text=haber.detay;
                imgHaber.ImageUrl = "/uploads/haber/" + haber.resim;
            }
        }
    }
}