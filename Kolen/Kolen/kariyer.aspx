﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Kolen.Master" AutoEventWireup="true" CodeBehind="kariyer.aspx.cs" Inherits="Kolen.kariyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="page-baslik">
                    KARİYER
                </div>

                <div class="page-baslik mt10 page-icerik">
                     <asp:Literal Text="" ID="lblUyari" runat="server" />
            <table class="table table-kariyer">
                <tr>
                    <td style="width: 120px;">Adı</td>
                    <td>:</td>
                    <td style="width: 250px;">
                        <asp:TextBox runat="server" ID="txtAdi" /></td>
                    <td style="width: 120px;">Soyadı</td>
                    <td>:</td>
                    <td style="width: 250px;">
                        <asp:TextBox runat="server" ID="txtSoyadi" /></td>
                </tr>
                <tr class="bg-co-tr">
                    <td>Telefon</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtTel" /></td>
                    <td>Cep Tel</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtCep" /></td>
                </tr>
                <tr>
                    <td>E-Posta</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtEposta" /></td>
                    <td>Başvuru Tipi</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtBasvuruTipi" /></td>
                </tr>
                <tr class="bg-co-tr">
                    <td>Doğumtarihi</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtDogumTarihi" /></td>
                    <td>Cinsiyeti</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtCinsiyet" /></td>
                </tr>
                <tr>
                    <td>İkamet Ettiği il</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtIkametIl" /></td>
                    <td>Mezun olunan<br />
                        Okul/Bölüm</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtOkul" /></td>
                </tr>
                <tr class="bg-co-tr">
                    <td>İş Yeri</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtIsyeri" /></td>
                    <td>Görev</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtGorev" /></td>
                </tr>
                <tr>
                    <td>İşe Başlama T.</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtIseBaslamaTarihi" /></td>
                    <td>İş bitiş T.</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtIsBitisTarihi" /></td>
                </tr>
                <tr class="bg-co-tr">
                    <td>Mesajınız</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtMesaj" TextMode="MultiLine" CssClass="mesaj" Rows="5" /></td>
                    <td>CV Ekle</td>
                    <td>:</td>
                    <td><asp:FileUpload ID="flResimYukle" runat="server" CssClass="file-upload" OnLoad="flResimYukle_Load"/> </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <asp:Button Text="Gönder" CssClass="btn btn-primary btn-width" ID="btnGonder" runat="server" OnClick="btnGonder_Click" /></td>
                </tr>
            </table>

                </div>

            </div>
        </div>
    </div>
</asp:Content>
