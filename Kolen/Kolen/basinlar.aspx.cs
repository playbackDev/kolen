﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen
{
    public partial class basinlar :cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            rptBasinlar.BindVeri(db.kolen_basin.Where(o => o.is_active == true).OrderBy(s=>s.create_date).ToList());
        }
    }
}