﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="uyari.aspx.cs" Inherits="Kolen.uyari" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href='http://fonts.googleapis.com/css?family=Dosis:400,600,700,800&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <script src="/assets/js/jquery-1.8.2.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>

</head>
<body>
    <form id="form1" runat="server">
     <div class="container">
        <div class="row">
            <div class="span6 offset3" style="margin-top: 100px;">
                
                        <asp:Literal ID="ltrUyari" runat="server" />
                   
            </div>
        </div>
    </div>
    </form>
</body>
</html>