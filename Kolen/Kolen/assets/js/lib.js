$(document).ready(function(){
	
	$('#shape-1').delay(750).fadeIn(2000);
	$('#shape-2').delay(1350).fadeIn(2000);
	$('section#hakkimizda').waypoint(function(event, direction) {
		if(direction === 'down'){
			$('#shape-3').fadeIn(2000);
		}
	});
	$('section#cupcake').waypoint(function(event, direction) {
		if(direction === 'down'){
			$('#shape-4').fadeIn(2000);
		}
	});
	$('section#skills').waypoint(function(event, direction) {
		if(direction === 'down'){
			$('#shape-5').fadeIn(2000);
			$('#shape-6').delay(500).fadeIn(2000);
		}
	});
	$('section#contact').waypoint(function(event, direction) {
		if(direction === 'down'){
			$('#shape-7').fadeIn(2000);
		}
	});
	$('section#info').waypoint(function(event, direction) {
		if(direction === 'down'){
			$('#shape-8').fadeIn(2000);
		}
	});
	
	/* #################################
	 * WORKSLIDER: create slide-elements
	 * #################################
	 */
	$('#workList li').each(function(){
		srcTmp = $(this).find('img').attr('src');
		$(this).find('img').hide();
		$('.workListInfo', this).before('<div style="background:url('+srcTmp+') left top" class="sw">');
		$('.workListInfo', this).after('<div style="background:url('+srcTmp+') left bottom" class="color">');
	})
	
	$('#workList li a').each(function(){
		$(this).hover(function(){
			$('.sw', this).stop().animate({opacity:'0'}, 500);
		}, function(){
			$('.sw', this).stop(false, true).animate({opacity:'1'}, 500);
		});
	});
	

	$("#workList li a").click(function(event) {
		event.preventDefault();
		linkLocation = this.href;
		$("body").fadeOut(1000, redirectPage);
	});

	function redirectPage() {
		window.location = linkLocation;
	}
	
	/*
	 * #####################
	 * ABOUT: timeline hover
	 * #####################
	 */
	$('#aboutTimeline li').each(function(){
		$(this).hover(function(){
			var aboutTimelineTmp = $(this).attr('id');
			$('#aboutTimelineText'+' .'+aboutTimelineTmp).stop().animate({
				opacity: '1',
				top: '0'
			}, 500);
		}, function(){
			var aboutTimelineTmp = $(this).attr('id');
			$('#aboutTimelineText'+' .'+aboutTimelineTmp).stop(true, false).animate({
				opacity: '0',
				top: '-30px'
			}, 500);
		});
	});
	
	/* ############# */
	/* Smooth Scroll */
	/* ############# */
	function filterPath(string) {
		return string
		.replace(/^\//,'')
		.replace(/(index|default).[a-zA-Z]{3,4}$/,'')
		.replace(/\/$/,'');
	}
	var locationPath = filterPath(location.pathname);
	var scrollElem = scrollableElement('html', 'body');
	$('a[href*=#]').each(function() {
		var thisPath = filterPath(this.pathname) || locationPath;
		if (  locationPath == thisPath
			&& (location.hostname == this.hostname || !this.hostname)
			&& this.hash.replace(/#/,'') ) {
				var $target = $(this.hash), target = this.hash;
				if (target) {
					var targetOffset = $target.offset().top;
					$(this).click(function(event) {
						event.preventDefault();
						$(scrollElem).animate({scrollTop: targetOffset}, 400, function() {
							location.hash = target;
						});
					});
				}
		}
	});
	function scrollableElement(els) {
		for (var i = 0, argLength = arguments.length; i <argLength; i++) {
			var el = arguments[i],
			$scrollElement = $(el);
			if ($scrollElement.scrollTop()> 0) {
				return el;
			} else {
				$scrollElement.scrollTop(1);
				var isScrollable = $scrollElement.scrollTop()> 0;
				$scrollElement.scrollTop(0);
				if (isScrollable) {
					return el;
				}
			}
		}
		return [];
	}
	
	$('#contactform #Name').val('Name:');
	$checkEMail = $('#contactform #EMail').val('E-Mail:');
	$('#contactform #Nachricht').text('Nachricht:');

	checkName = $('#contactform #Name').val();
	checkEMail = $('#contactform #EMail').val();
	checkNachricht = $('#contactform #Nachricht').text();	

	$('.form_field').focus(function(){
		$(this).addClass('contactFormFocus');
		checkTmpSec = 'check'+$(this).attr('id');
		if($(this).val() == window[checkTmpSec]){
			$(this).val('');
		}
	});
	
	$('.form_field').blur(function(){
		if($(this).val() == ""){
			$(this).removeClass('contactFormFocus');
			checkTmp = 'check'+$(this).attr('id');
			$(this).val(window[checkTmp]);
		};
	});
	
	$('#contactform #submit').click(function(clickEvent){
		clickEvent.preventDefault();
		var Name = $('input[name=Name]');
		var EMail = $('input[name=EMail]');
		var Nachricht = $('textarea[name=Nachricht]');
		var aspm = $('input[name=aspm]');		
		var data =
		'Name=' + Name.val() + 
		'&EMail=' + EMail.val() +
		'&aspm=' + aspm.val() +
		'&Nachricht=' + encodeURIComponent(Nachricht.val());
		
		if(checkName == $('#contactform #Name').val()){
			alert('Bitte alle Felder ausfüllen');
		}else if(checkEMail == $('#contactform #EMail').val()){
			alert('Bitte alle Felder ausfüllen');
		}else if(checkNachricht == $('#contactform #Nachricht').val()){
			alert('Bitte alle Felder ausfüllen');
		}else{
			$.ajax({
				url: "http://design-addicted.de/sendmail.php",
				type: "POST",
				data: data,
				success: function(data){
					console.log(data);
					if(data == '1'){
						$('#formMessage').animate({opacity: '1'}, 500);
						$('<p class="formMessageHeadline">Vielen Dank!</p><p class="formMessageText">Ich habe deine Nachricht erhalten und werde mich schnellstens bei Dir melden.</p>').appendTo('#formMessage');
					}else{
						alert('Etwas hat nicht geklappt, bitte nutzen Sie die unten angegebene E-Mail-Adresse.');
					}
				}
			});
		};
	});
	
	
	$('.trigger').not('.trigger_active').next('.toggle_container').hide();
	$('.trigger').click( function() {
		var trig = $(this);
		if ( trig.hasClass('trigger_active') ) {
			trig.next('.toggle_container').slideToggle('slow');
			trig.removeClass('trigger_active');
		} else {
			$('.trigger_active').next('.toggle_container').slideToggle('slow');
			$('.trigger_active').removeClass('trigger_active');
			trig.next('.toggle_container').slideToggle('slow');
			trig.addClass('trigger_active');
		};
		return false;
	});
	
})
