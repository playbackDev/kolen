﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;


namespace Kolen
{
    public class cMember : cBase
    {
        public bool adminLogin(string username, string Password)
        {
            Password = Password.ToMD5();
            var login = (from c in db.kolen_control_panel
                         where c.username == username && c.password == Password
                         select new
                         {
                             c.id,
                             c.username,
                             c.role

                         }).FirstOrDefault();

            if (login == null)
                return false;
            else
            {
                HttpContext.Current.Session["admin_login"] = true;
                HttpContext.Current.Session["admin_id"] = login.id;
                HttpContext.Current.Session["admin_username"] = login.username;
                HttpContext.Current.Session["role"] = login.role;
                return true;
            }
        }

        public bool adminIsLogged()
        {

            try
            {
                if (HttpContext.Current.Session["admin_login"] == null)
                {
                    if (HttpContext.Current.Request.Cookies["kol"] != null)
                    {
                        string cookie_content = HttpContext.Current.Request.Cookies["kol"].Value;

                        if (cookie_content != "")
                        {
                            cookie_content = cEncrytion.DecryptString(cookie_content);

                            if (cookie_content.Contains("-ls-"))
                                if (adminLogin(Regex.Split(cookie_content, "-ls-")[0], Regex.Split(cookie_content, "-ls-")[1]))
                                    return true;
                                else
                                    return false;
                            else
                                return false;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    if (Convert.ToBoolean(HttpContext.Current.Session["admin_login"]))
                        return true;
                    else
                        return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}