﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen
{
    public partial class uyari : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string uyr = "";
            string msg;
            if (!Request.QueryString["durum"].IsTextBoxEmptyOrNull())
            {

                string durum = Request.QueryString["durum"];
                string syf = Request.QueryString["syf"];

                if (durum == "basarili")
                {
                    uyr = "Mesajınız başarıyla gönderilmiştir.";
                }
                else if (durum == "hata")
                {

                    uyr = "Mesaj gönderilemedi.";
                }


                msg = "<div class='alert alert-success'>" +
                   "<button type='button' class='close' data-dismiss='alert'>&times;</button>" +
                   "<h3>Uyarı!</h3>" +
                   "<h5>"
                   + uyr +
                   "</h5>" +
                   "<a href='/" + syf + ".aspx' class='btn btn-success'>Tamam</a>" +
               "</div>";

                ltrUyari.Text = msg;
            }
        }
    }
}