﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Kolen.Master" AutoEventWireup="true" CodeBehind="iletisim.aspx.cs" Inherits="Kolen.iletisim"  MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        #map-canvas {
            width: 774px;
            height: 486px;
        }
    </style>
     
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
     <script>
         function initialize() {
             var myLatlng = new google.maps.LatLng(39.898588,32.872593);
             var mapOptions = {
                 zoom: 18,
                 center: myLatlng,
                 mapTypeId: google.maps.MapTypeId.ROADMAP
             }

             var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

             var contentString = '<div id="content">' +
                'KOLEN'+
                 '</div>';

             var infowindow = new google.maps.InfoWindow({
                 content: contentString
             });

             var marker = new google.maps.Marker({
                 position: myLatlng,
                 map: map,
                 title: 'KOLEN'
             });
             google.maps.event.addListener(marker, 'click', function () {
                 infowindow.open(map, marker);
             });
         }

         google.maps.event.addDomListener(window, 'load', initialize);

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
       
        <div class="row">
            <div class="span12">
                <div class="page-baslik">
                    İLETİŞİM
                </div>

                <div class="page-baslik mt10 page-icerik">
                <div class="harita">
                    <div id="map-canvas"></div>
                </div>
                    <div class="row">

                    <div class="span6">
                        <asp:Label Text="" runat="server" ID="lblUyari"/>
                      
                        <table class="table table-kariyer">
                            <tr class="bg-co-tr1">
                                <td style="width: 150px">Ad Soyad</td>
                                <td style="width: 50px">:</td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtAdSoyad" placeholder="Ad Soyad Giriniz" /></td>
                            </tr>
                            <tr class="bg-co-tr2">
                                <td>E-posta</td>
                                <td>:</td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtEPosta" placeholder="E-mail giriniz" />
                                </td>
                            </tr>
                            <tr class="bg-co-tr1">
                                <td>Telefon</td>
                                <td>:</td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTel" placeholder="Telefon numaranız" />
                                </td>
                            </tr>
                            <tr class="bg-co-tr2">
                                <td>Konu</td>
                                <td>:</td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtKonu" placeholder="ileti konusu" />
                                </td>
                            </tr>
                            <tr class="bg-co-tr1">
                                <td>Mesaj</td>
                                <td>:</td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtMesaj" TextMode="MultiLine" placeholder="mesajınız." Rows="7" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>
                                    <asp:Button ID="btnGonder" CssClass="btn btn-private ml150" Text="Gönder" runat="server" Style="margin-bottom: 12px;" OnClick="btnGonder_Click" /></td>
                            </tr>
                        </table>
                    </div>
                        <div class="span5">
                        <table class="address">
                             <tr>
                                <td class="kalin">Çağrı Merkezi</td>
                                <td>:</td>
                                <td>444 9 556</td>
                            </tr>
                            <tr>
                                <td class="kalin">Adres</td>
                                <td>:</td>
                                <td> Horasan sk. No: 16/3 G.O.P. Çankaya, Ankara</td>
                            </tr>
                            <tr>
                                <td class="kalin">Tel</td>
                                <td>:</td>
                                <td>+90 312 447 17 00</td>
                            </tr>
                            <tr>
                                <td class="kalin">Faks</td>
                                <td>:</td>
                                <td>+90 312 446 24 80</td>
                            </tr>
                        </table>
                    </div>
                    </div>
                    

                </div>
 
            </div>
        </div>
    </div>
</asp:Content>
