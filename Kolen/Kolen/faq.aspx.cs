﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen
{
    public partial class faq : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var faq = db.kolen_bilgiler.OrderBy(s => s.create_date).ToList();
            rptSorular.DataSource = faq;
            rptSorular.DataBind();
        }
    }
}