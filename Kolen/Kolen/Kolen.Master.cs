﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen
{
    public partial class Kolen : System.Web.UI.MasterPage
    {
        cBase cb = new cBase();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var modal = cb.db.kolen_modal.Where(s => s.aktif == 1).FirstOrDefault();
                if (modal != null)
                {
                    if (Session["modal_cont"] != "true")
                    {
                        Session["modal_cont"] = "true";
                        string str =
                            "<script type='text/javascript' charset='utf-8'>" +
                            "jQuery(function ($) {" +
                            "$('#basic-modal-content').modal();" +
                            "});" +
                            "</script>";
                        ltrScriptModal.Text = str;
                    }
                    else
                    {
                        ltrScript.Text = null;
                    }
                    ltrModalResim.Text = "<img src='/uploads/modal/" + modal.resim + "' alt='' />";
                }


                var arkaPlanResimleri = cb.db.kolen_bg_images.OrderBy(o => o.sira).ToList();
                string backPlans = "";
                foreach (var item in arkaPlanResimleri)
                {
                    backPlans += "'/uploads/mainBG/" + item.img_path + "',";
                }
                backPlans = backPlans.TrimEnd(',');
                string arkaPlan = "<script> $.backstretch([" + backPlans + " ], { duration: 4000, fade: 750 });</script>";
                ltrScript.Text = arkaPlan;
            }
        }
    }
}