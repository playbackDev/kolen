﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Kolen.Master" AutoEventWireup="true" CodeBehind="haber.aspx.cs" Inherits="Kolen.haber" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="page-baslik">
                    HABER
                </div>
                <div class="page-baslik mt10 page-icerik">
                   <div class="haber_cont">
                       <div class="row">
                           <div class="span3">
                               <asp:Image ID="imgHaber" runat="server" />
                           </div>
                           <div class="span7"> 
                               <p><asp:Literal ID="ltrBaslik" runat="server" /></p>
                               <p><asp:Literal ID="ltrIcerik" runat="server" /></p>

                           </div>
                       </div>
                     
                   </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
