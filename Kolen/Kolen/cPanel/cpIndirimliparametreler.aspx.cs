﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen.cPanel
{
    public partial class cpIndirimliparametreler : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var liste = db.kolen_indirimlihesap.ToList();
                rptKayit.DataSource = liste;
                rptKayit.DataBind();

                var sabit = db.kolen_indhesap_sabitler.FirstOrDefault();
                txtTRTpayi.Text = sabit.trtpayi;
                txtEnerjifonu.Text = sabit.enerjifonu;
                txtBTV.Text = sabit.btv;
            }

        }

        protected void rptKayit_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            TextBox txtFirma = (TextBox)e.Item.FindControl("txtFirma");
            txtFirma.Text = DataBinder.Eval(e.Item.DataItem, "firma").ToString();
            txtFirma.Enabled = false;

            TextBox txtTip = (TextBox)e.Item.FindControl("txtTip");
            txtTip.Text = DataBinder.Eval(e.Item.DataItem, "tip").ToString();
            txtTip.Enabled = false;

            TextBox txtPeSaBedeli = (TextBox)e.Item.FindControl("txtPeSaBedeli");
            txtPeSaBedeli.Text = DataBinder.Eval(e.Item.DataItem, "per_sat_bed").ToString();

            TextBox txtKayip = (TextBox)e.Item.FindControl("txtKayip");
            txtKayip.Text = DataBinder.Eval(e.Item.DataItem, "kayip").ToString();

            TextBox txtDagitim = (TextBox)e.Item.FindControl("txtDagitim");
            txtDagitim.Text = DataBinder.Eval(e.Item.DataItem, "dagitim").ToString();

            TextBox txtPSH = (TextBox)e.Item.FindControl("txtPSH");
            txtPSH.Text = DataBinder.Eval(e.Item.DataItem, "psh").ToString();

            TextBox txtIletim = (TextBox)e.Item.FindControl("txtIletim");
            txtIletim.Text = DataBinder.Eval(e.Item.DataItem, "iletim").ToString();

        }

        protected void lnkAktif_Command(object sender, CommandEventArgs e)
        {


        }

        protected void rptKayit_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
           
                int id = Convert.ToInt32(e.CommandArgument);

                var kayit = db.kolen_indirimlihesap.Where(s => s.id == id).FirstOrDefault();

                TextBox txtPeSaBedeli = (TextBox)e.Item.FindControl("txtPeSaBedeli");
                kayit.per_sat_bed = txtPeSaBedeli.Text;



                TextBox txtKayip = (TextBox)e.Item.FindControl("txtKayip");
                kayit.kayip = txtKayip.Text;

                TextBox txtDagitim = (TextBox)e.Item.FindControl("txtDagitim");
                kayit.dagitim = txtDagitim.Text;


                TextBox txtPSH = (TextBox)e.Item.FindControl("txtPSH");
                kayit.psh = txtPSH.Text;

                TextBox txtIletim = (TextBox)e.Item.FindControl("txtIletim");
                kayit.iletim = txtIletim.Text;

                if (db.SaveChanges() > 0)
                    RefreshPage();

            }

        protected void btnSabitGuncelle_Click(object sender, EventArgs e)
        {
            var sabit=db.kolen_indhesap_sabitler.FirstOrDefault();
            if (!txtTRTpayi.Text.IsTextBoxEmptyOrNull())
                sabit.trtpayi = txtTRTpayi.Text;
            if (!txtEnerjifonu.Text.IsTextBoxEmptyOrNull())
                sabit.enerjifonu = txtEnerjifonu.Text;
            if (!txtBTV.Text.IsTextBoxEmptyOrNull())
                sabit.btv = txtBTV.Text;

            if (db.SaveChanges()>0)
                RefreshPage();
        }
        
    }
}