﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen.cPanel
{
    public partial class background : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var resimler = db.kolen_bg_images.OrderBy(p => p.sira).ToList();
            if (!Page.IsPostBack)
            {
                if (resimler != null)
                {
                    rptBackResimleri.DataSource = resimler;
                    rptBackResimleri.DataBind();
                }
            }
        }

        protected void rptBackResimleri_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Literal input = (Literal)e.Item.FindControl("ltrInput");
            input.Text = "<input type=\"text\" name=\"txtSira" + DataBinder.Eval(e.Item.DataItem, "id") + "\" id=\"txtSira" + DataBinder.Eval(e.Item.DataItem, "id") + "\" class=\"span1\" style='margin:0px;' value=\"" + DataBinder.Eval(e.Item.DataItem, "sira") + "\" />";
        }

        protected void btnBackResim_Click(object sender, EventArgs e)
        {
            kolen_bg_images yeniResim = new kolen_bg_images();
            string random_name = CreateRandomValue(10, true, true, true, false) + ".jpg";
            if (fileResim.HasFile)
            {
                pnlGeneralError.Visible = false;
                try
                {
                    fileResim.SaveAs(Server.MapPath("~/uploads/mainBG/" + random_name));
                    //ResizeImage("~/uploads/mainBG/" + random_name, 230, 230);
                    yeniResim.img_path = random_name;
                    yeniResim.sira = 1;
                    db.kolen_bg_images.Add(yeniResim);
                    if (db.SaveChanges() > 0)
                    {
                        RefreshPage();
                    }
                }
                catch (Exception ex)
                {
                    ltrErrorText.Text = "Hata: " + ex.Message;
                    pnlGeneralError.Visible = true;
                }
            }
            else
            {
                ltrErrorText.Text = "Lütfen  resim seçiniz.";
                pnlGeneralError.Visible = true;
            }
        }

        protected void lnkSirala_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int sira = Convert.ToInt32(Request.Form["txtSira" + e.CommandArgument]);
                int id = Convert.ToInt32(e.CommandArgument);
                var sirala = db.kolen_bg_images.Where(o => o.id == id).FirstOrDefault();
                sirala.sira = sira;
                db.SaveChanges();
            }
            catch { }
            finally
            {
                cBase.RefreshPage();
            }
        }

        protected void lnkSil_Command(object sender, CommandEventArgs e)
        {
            int silResim = Convert.ToInt32(e.CommandArgument);
            var silinecekResim = db.kolen_bg_images.Where(i => i.id == silResim).FirstOrDefault();
            db.kolen_bg_images.Remove(silinecekResim);
            if (db.SaveChanges() > 0)
            {
                try
                {
                    File.Delete(Server.MapPath("~/uploads/mainBG/" + silinecekResim.img_path));
                }
                catch (Exception ex)
                {
                    ltrErrorText.Text = "Hata: " + ex.Message;
                    pnlGeneralError.Visible = true;
                }
                finally { RefreshPage(); }
            }
        }
    }
}