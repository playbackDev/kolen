﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen
{
    public partial class cpBasinlar :cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                rptHaberler.BindVeri(db.kolen_basin.OrderBy(o => o.create_date).ToList());
            }

        }

        protected void rptHaberler_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkAktif = (LinkButton)e.Item.FindControl("lnkAktif");
            bool aktifMi = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "is_active"));
            if (aktifMi)
            {
                lnkAktif.Text = "Aktif";
                lnkAktif.CssClass = "btn btn-success";
            }
            else
            {
                lnkAktif.Text = "Pasif";
                lnkAktif.CssClass = "btn btn-danger";
            }

        }

        protected void lnkAktif_Command(object sender, CommandEventArgs e)
        {
            int id = Convert.ToInt32(e.CommandArgument);
            var aktifMi = db.kolen_basin.Where(o => o.id == id).FirstOrDefault();
            if (Convert.ToBoolean(aktifMi.is_active))
            {
                aktifMi.is_active = false;
            }
            else
            {
                aktifMi.is_active = true;
            }
            if (db.SaveChanges() > 0)
            {
                RefreshPage();
            }

        }
    }
}