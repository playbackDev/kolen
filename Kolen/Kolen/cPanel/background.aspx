﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cPanel/cPanel.Master" AutoEventWireup="true" CodeBehind="background.aspx.cs" Inherits="Kolen.cPanel.background" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
      <h5>
        <asp:Literal ID="ltrNeResimleri" Text="" runat="server" />
        Resimleri</h5>
       <asp:Panel runat="server" CssClass="alert alert-error" Visible="false" ID="pnlGeneralError">
        <asp:Literal ID="ltrErrorText" runat="server" />
    </asp:Panel>

       <div class="well">
        <button type="button" class="close" data-dismiss="alert" style="font-size:16px;">&times;Kapat</button>
        <strong>Not: </strong> Arka plan resmi bilgisayar ekranlarına göre boyutlandığından, büyük ebatlarda resim tercih edilmelidir.
      
</div>

    <table class="table table-bordered table-striped">
        <thead>

            <tr>
                <th style="text-align: center; vertical-align: middle; width: 10%;">Yeni Resim Yükle</th>
                <th style="text-align: center; vertical-align: middle; width: 10%;">
                    <asp:FileUpload ID="fileResim" runat="server" />
                </th>
                <th style="text-align: center; vertical-align: middle; width: 10%;">
                    <asp:Button ID="btnBackResim" Text="Yükle" runat="server" CssClass="btn btn-success" OnClick="btnBackResim_Click" />
                </th>

            </tr>

        </thead>
    </table>

    <div class="span12 basin">
        <div id="s1">
            <asp:Repeater ID="rptBackResimleri" runat="server" OnItemDataBound="rptBackResimleri_ItemDataBound">
                <ItemTemplate>
                    <div style="position: relative; float: left; width: 200px; height: 174px; margin-right: 45px; margin-bottom: 10px;">
                        <asp:LinkButton ID="lnkSil" CssClass="silme" runat="server" CommandName="sil" CommandArgument='<%#Eval("id") %>' Style="position: absolute; text-decoration: none; bottom: 0px; background-color: white;" OnCommand="lnkSil_Command" ToolTip="Sil">  <i class="icon-remove icon-large"></i> Sil &nbsp;</asp:LinkButton>
                        <img class="silik" src="/uploads/mainBG/<%#Eval("img_path") %>" />
                        <div style="position: absolute; bottom: -2px; left: 58px;">
                            <asp:Literal ID="ltrInput" Text="" runat="server" />
                            <asp:LinkButton ID="lnkSirala" runat="server" OnCommand="lnkSirala_Command" CssClass="btn btn-info" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "id") %>'>Sırala</asp:LinkButton>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>


        </div>
    </div>

</asp:Content>
