﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen.cPanel
{
    public partial class cpBilgi_duzenle : cBase
    {
        int bilgiID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].IsNumeric())
            {
                bilgiID = Convert.ToInt32(Request.QueryString["id"]);
                if (!Page.IsPostBack)
                {
                    var bilgi = db.kolen_bilgiler.Where(o => o.id == bilgiID).FirstOrDefault();
                    txtBaslik.Text = bilgi.baslik;
                    txtKisa.Text = bilgi.kisa_yazi;
                    txtOzellik.Text = bilgi.detay;
                }
            }
            else
            {
                Response.Redirect("cpBilgiler.aspx", false);
            }
        }

        protected void btnHaberDuzenle_Click(object sender, EventArgs e)
        {
            var bilgi = db.kolen_bilgiler.Where(o => o.id == bilgiID).FirstOrDefault();
            if (txtBaslik.Text != "")
            {
                bilgi.baslik = txtBaslik.Text;

            }
            if (txtKisa.Text != "")
            {
                bilgi.kisa_yazi = txtKisa.Text;

            }
            if (txtOzellik.Text != "")
            {
                bilgi.detay = txtOzellik.Text;
            }

            if (db.SaveChanges() > 0)
            {
                Response.Redirect("cpBilgiler.aspx");
            }

        }
    }
}