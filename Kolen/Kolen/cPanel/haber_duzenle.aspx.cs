﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen.cPanel
{
    public partial class haber_duzenle : cBase
    {
        int haberID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].IsNumeric())
            {
                haberID = Convert.ToInt32(Request.QueryString["id"]);
                if (!Page.IsPostBack)
                {
                    var haber = db.kolen_news.Where(o => o.id == haberID).FirstOrDefault();
                    txtBaslik.Text = haber.baslik;
                    txtKisa.Text = haber.kisa_yazi;
                    txtOzellik.Text = haber.detay;
                }
            }
            else
            {
                Response.Redirect("habers.aspx", false);
            }
        }

        protected void btnHaberDuzenle_Click(object sender, EventArgs e)
        {
            var haber = db.kolen_news.Where(o => o.id == haberID).FirstOrDefault();
            if (txtBaslik.Text!="")
            {
                haber.baslik = txtBaslik.Text;

            }
            if (txtKisa.Text!="")
            {
                haber.kisa_yazi = txtKisa.Text;

            }
            if (txtOzellik.Text!="")
            {
                haber.detay = txtOzellik.Text;
            }

            if (fileHaberAnasayfa.HasFile)
            {
                string random_name = CreateRandomValue(10, true, true, true, false) + ".jpg";

                try
                {
                    fileHaberAnasayfa.SaveAs(Server.MapPath("~/uploads/haber/" + random_name));
                    ResizeImage("~/uploads/haber/" + random_name, 220, 200);
                    haber.resim = random_name;
                }
                catch (Exception ex)
                {
                }

            }

            if (db.SaveChanges()>0)
            {
                Response.Redirect("habers.aspx");
            }

        }
    }
}