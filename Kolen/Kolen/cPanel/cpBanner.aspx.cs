﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen.cPanel
{
    public partial class cpBanner :cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var banner = db.kolen_banner.OrderBy(p => p.sira).ToList();
            if (!Page.IsPostBack)
            {
                if (banner != null)
                {
                    rptBackResimleri.DataSource = banner;
                    rptBackResimleri.DataBind();
                }
            }
        }

        protected void rptBackResimleri_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Literal ltrYazi = (Literal)e.Item.FindControl("ltrYazi");
            ltrYazi.Text = DataBinder.Eval(e.Item.DataItem, "baslik").ToString();
            Literal input = (Literal)e.Item.FindControl("ltrInput");
            input.Text = "<input type=\"text\" name=\"txtSira" + DataBinder.Eval(e.Item.DataItem, "id") + "\" id=\"txtSira" + DataBinder.Eval(e.Item.DataItem, "id") + "\" class=\"span1\" style='margin:0px;' value=\"" + DataBinder.Eval(e.Item.DataItem, "sira") + "\" />";
        }

        protected void btnBackResim_Click(object sender, EventArgs e)
        {
            kolen_banner yeniResim = new kolen_banner();
                pnlGeneralError.Visible = false;
                try
                {
                    yeniResim.baslik=txtBannerYazi.Text;
                    yeniResim.adi = txtBannerIcerik.Text;
                    yeniResim.sira = 1;
                    yeniResim.link = txtBannerLink.Text;
                    db.kolen_banner.Add(yeniResim);

                    if (db.SaveChanges() > 0)
                    {
                        RefreshPage();
                    }
                }
                catch (Exception ex)
                {
                    ltrErrorText.Text = "Hata: " + ex.Message;
                    pnlGeneralError.Visible = true;
                }
            }
  
        protected void lnkSirala_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int sira = Convert.ToInt32(Request.Form["txtSira" + e.CommandArgument]);
                int id = Convert.ToInt32(e.CommandArgument);
                var sirala = db.kolen_banner.Where(o => o.id == id).FirstOrDefault();
                sirala.sira = sira;
                db.SaveChanges();
            }
            catch 
            { 
            
            }
            finally
            {
                cBase.RefreshPage();
            }
        }

        protected void lnkSil_Command(object sender, CommandEventArgs e)
        {
            int silResim = Convert.ToInt32(e.CommandArgument);
            var silinecekResim = db.kolen_banner.Where(i => i.id == silResim).FirstOrDefault();
            db.kolen_banner.Remove(silinecekResim);
            db.SaveChanges();
            RefreshPage();
        }
    }
}