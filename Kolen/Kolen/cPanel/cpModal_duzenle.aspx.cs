﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen.cPanel
{
    public partial class cpModal_duzenle : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var modal = db.kolen_modal.FirstOrDefault();
                if (modal != null)
                {
                    imgModal.ImageUrl = "/uploads/modal/" + modal.resim;
                    if (modal.aktif == 1)
                        chkAktif.Checked = true;
                    else
                        chkAktif.Checked = false;
                }
            }

        }

        protected void btnAnaSlide_Click(object sender, EventArgs e)
        {
            var modal = db.kolen_modal.FirstOrDefault();

            if (chkAktif.Checked)
                modal.aktif = 1;
            else
                modal.aktif = 0;

            string randomname = CreateRandomValue(10, true, true, true, false);
            if (flArsivSlide.HasFile)
            {
                try
                {
                    flArsivSlide.SaveAs(Server.MapPath("~/uploads/modal/" + randomname + ".jpg"));
                    ResizeImage("~/uploads/modal/" + randomname + ".jpg", 700, 400);
                    modal.resim = randomname + ".jpg";
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
            }

            if (db.SaveChanges() > 0)
            {
                RefreshPage();
            }



        }
    }
}