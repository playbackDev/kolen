﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cPanel/cPanel.Master" AutoEventWireup="true" CodeBehind="cpIndirimliparametreler.aspx.cs" Inherits="Kolen.cPanel.cpIndirimliparametreler" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        input[type="text"] {
        width:90px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Firma</th>
                <th>Tip</th>
                <th>Pe. Sat. Bedeli</th>
                <th>kayip</th>
                <th>Dağıtım</th>
                <th>PSH</th>
                <th>İletim</th>
                <th>İşlem</th>
            </tr>
        </thead>
        <tbody>
            <asp:Repeater runat="server" ID="rptKayit" OnItemDataBound="rptKayit_ItemDataBound" OnItemCommand="rptKayit_ItemCommand">
                <ItemTemplate>
             <tr>
                <td><asp:TextBox runat="server" ID="txtFirma" /> </td>
                <td><asp:TextBox runat="server" ID="txtTip" /> </td>
                <td><asp:TextBox runat="server" ID="txtPeSaBedeli" /> </td>
                <td><asp:TextBox runat="server" ID="txtKayip" /> </td>
                <td><asp:TextBox runat="server" ID="txtDagitim" /> </td>
                <td><asp:TextBox runat="server" ID="txtPSH" /> </td>
                <td><asp:TextBox runat="server" ID="txtIletim" /> </td>
                 <td><asp:LinkButton CssClass="btn btn-success" ID="lnkAktif" Text="Değiştir" runat="server" CommandArgument='<%#Eval("id") %>' OnCommand="lnkAktif_Command"/></td>
            </tr>
                </ItemTemplate>
            </asp:Repeater>
            
        </tbody>
    </table>

    <table class="table table-bordered">
        <thead>
            <tr>
                <th>TRT Payı</th>
                <th>Enerji Fonu</th>
                <th>BTV</th>
                <th>İşlem</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><asp:TextBox runat="server" ID="txtTRTpayi" /></td>
                <td><asp:TextBox runat="server" ID="txtEnerjifonu" /></td>
                <td><asp:TextBox runat="server" ID="txtBTV" /></td>
                <td><asp:Button Text="Güncelle" runat="server" ID="btnSabitGuncelle" OnClick="btnSabitGuncelle_Click" /></td>
            </tr>
        </tbody>
    </table>
</asp:Content>
