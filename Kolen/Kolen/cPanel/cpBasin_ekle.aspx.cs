﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen.cPanel
{
    public partial class cpBasin_ekle :cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnHaberEkle_Click(object sender, EventArgs e)
        {
            kolen_basin yeniBasin = new kolen_basin();
            if (txtBaslik.Text != "")
            {
                pnlGeneralError.Visible = false;
                yeniBasin.baslik = txtBaslik.Text;
                if (txtKisa.Text != "")
                {
                    yeniBasin.kisa_yazi = txtKisa.Text;
                    pnlGeneralError.Visible = false;
                    if (txtOzellik.Text != "")
                    {
                        yeniBasin.detay = txtOzellik.Text;
                        yeniBasin.is_active = false;
                        yeniBasin.create_date = DateTime.Now;
                        db.kolen_basin.Add(yeniBasin);
                        if (db.SaveChanges() > 0)
                        {
                            Response.Redirect("cpBasinlar.aspx");
                        }
                    }
                    else
                    {
                        ltrErrorText.Text = "Lütfen Basın detayını girin.";
                        pnlGeneralError.Visible = true;
                    }
                }
                else
                {
                    ltrErrorText.Text = "Lütfen bir kısa yazı girin.";
                    pnlGeneralError.Visible = true;
                }
            }
            else
            {
                ltrErrorText.Text = "Lütfen bir haber başlığı girin.";
                pnlGeneralError.Visible = true;
            }
        }
    }
}