﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen.cPanel
{
    public partial class cpBilgi_ekle : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnHaberEkle_Click(object sender, EventArgs e)
        {
            kolen_bilgiler yeniHaber = new kolen_bilgiler();
            if (txtBaslik.Text != "")
            {
                pnlGeneralError.Visible = false;
                yeniHaber.baslik = txtBaslik.Text;
                if (txtKisa.Text != "")
                {
                    yeniHaber.kisa_yazi = txtKisa.Text;
                    pnlGeneralError.Visible = false;
                    if (txtOzellik.Text != "")
                    {
                        yeniHaber.detay = txtOzellik.Text;
                        yeniHaber.is_active = false;
                        yeniHaber.create_date = DateTime.Now;
                        db.kolen_bilgiler.Add(yeniHaber);
                        if (db.SaveChanges() > 0)
                        {
                            Response.Redirect("cpBilgiler.aspx");
                        }
                    }
                    else
                    {
                        ltrErrorText.Text = "Lütfen Bilgi detayını girin.";
                        pnlGeneralError.Visible = true;
                    }
                }
                else
                {
                    ltrErrorText.Text = "Lütfen bir kısa yazı girin.";
                    pnlGeneralError.Visible = true;
                }
            }
            else
            {
                ltrErrorText.Text = "Lütfen bir haber başlığı girin.";
                pnlGeneralError.Visible = true;
            }
        }
    }
}