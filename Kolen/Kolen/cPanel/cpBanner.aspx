﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cPanel/cPanel.Master" AutoEventWireup="true" CodeBehind="cpBanner.aspx.cs" Inherits="Kolen.cPanel.cpBanner" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       
      <h5>
        <asp:Literal ID="ltrNeResimleri" Text="" runat="server" />
        Banner Yazıları</h5>
       <asp:Panel runat="server" CssClass="alert alert-error" Visible="false" ID="pnlGeneralError">
        <asp:Literal ID="ltrErrorText" runat="server" />
    </asp:Panel>
       <div class="well">
        <button type="button" class="close" data-dismiss="alert" style="font-size:16px;">&times;Kapat</button>
        <strong>Not: </strong> Yazıların Çok uzun olmamasına dikkat ediniz.
</div>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th style="text-align: center; vertical-align: middle; width: 10%;">Yazı Başlık</th>
                <th style="text-align: center; vertical-align: middle; width: 10%;">
                    <asp:TextBox runat="server" ID="txtBannerYazi" />
                </th>
                <th>Maximum 75 karakter olmalı.</th>
                </tr>
            <tr>
                 <th style="text-align: center; vertical-align: middle; width: 10%;">Yazı İçerik</th>
                <th style="text-align: center; vertical-align: middle; width: 10%;">
                    <asp:TextBox runat="server" ID="txtBannerIcerik" />
                </th>
                 <th>Maximum 100 karakter olmalı.</th>
                 </tr>
            <tr>


                  <th style="text-align: center; vertical-align: middle; width: 10%;">Link girin</th>
                <th style="text-align: center; vertical-align: middle; width: 10%;">
                    <asp:TextBox runat="server" ID="txtBannerLink" />
                </th>
                 <th>Maximum 250 karakter olmalı.</th>
                 </tr>
            <tr>
                <th style="text-align: center; vertical-align: middle; width: 10%;">
                    
                </th>
                <th></th><th><asp:Button ID="btnBackResim" Text="Yükle" runat="server" CssClass="btn btn-success" OnClick="btnBackResim_Click" /></th>
            </tr>
        </thead>
    </table>

    <div class="span12 basin">
        <div id="s1">
            <asp:Repeater ID="rptBackResimleri" runat="server" OnItemDataBound="rptBackResimleri_ItemDataBound">
                <ItemTemplate>
                    <div style="position: relative; float: left; width: 400px; height: 100px; margin-right: 45px; margin-bottom: 10px; background-color:#d8cece; padding:10px;">
                        <asp:LinkButton ID="lnkSil" CssClass="silme" runat="server" CommandName="sil" CommandArgument='<%#Eval("id") %>' Style="position: absolute; text-decoration: none; bottom: 5px; background-color: white;" OnCommand="lnkSil_Command" ToolTip="Sil">  <i class="icon-remove icon-large"></i> Sil &nbsp;</asp:LinkButton>
                        <asp:Literal ID="ltrYazi" runat="server" />
                        <div style="position: absolute; bottom: 5px; left: 58px;">
                            <asp:Literal ID="ltrInput" Text="" runat="server" />
                            <asp:LinkButton ID="lnkSirala" runat="server" OnCommand="lnkSirala_Command" CssClass="btn btn-info" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "id") %>'>Sırala</asp:LinkButton>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>


        </div>
    </div>

</asp:Content>
