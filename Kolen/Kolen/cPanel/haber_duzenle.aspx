﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cPanel/cPanel.Master" AutoEventWireup="true" CodeBehind="haber_duzenle.aspx.cs" Inherits="Kolen.cPanel.haber_duzenle" ValidateRequest="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h5>Haber Düzenle  </h5>
    <asp:Panel runat="server" CssClass="alert alert-error" Visible="false" ID="pnlGeneralError">
        <asp:Literal ID="ltrErrorText" runat="server" />
    </asp:Panel>
    <div class="well">
        <button type="button" class="close" data-dismiss="alert" style="font-size: 16px;">&times;Kapat</button>
         <strong>Not: </strong> Kısa Yazı ve Başlık maximum karakter sayısı 150'tır <br />
    </div>
    <table class="table table-condensed">

        <tr>
            <td>Haber Başlık</td>
            <td>
                <asp:TextBox ID="txtBaslik" runat="server" MaxLength="150" /></td>
        </tr>
        <tr>
            <td>Kısa Yazı</td>
            <td>
                <asp:TextBox ID="txtKisa" runat="server" MaxLength="150" />
            </td>

        </tr>
       <tr>
            <td>Anasayfa Resmi</td>
            <td>
                <asp:FileUpload runat="server" ID="fileHaberAnasayfa"/>
            </td>

        </tr>
        <tr>
            <td>Haber İçerik</td>
            <td>
                <asp:TextBox ID="txtOzellik" runat="server" TextMode="MultiLine" />
            </td>

        </tr>
        <tr>
            <td>İşlem</td>
            <td>
                <asp:Button ID="btnHaberDuzenle" runat="server" CssClass="btn btn-success" Text="Kaydet" OnClick="btnHaberDuzenle_Click" /></td>
        </tr>

    </table>
</asp:Content>
