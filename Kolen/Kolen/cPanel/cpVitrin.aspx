﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cPanel/cPanel.Master" AutoEventWireup="true" CodeBehind="cpVitrin.aspx.cs" Inherits="Kolen.cPanel.cpVitrin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        input[type="text"] {
        
        width:600px;
        margin-bottom:0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>Vitrin Ayarları</h3>
    <table class="table">
        <thead>
            <tr><th colspan="3">Birinci Kısım</th></tr>
            <tr>
                <th>Yazı</th>
                <th><asp:TextBox CssClass="yazi-giris" runat="server" ID="txtYazi1"/></th>
                <th></th>
            </tr>
            <tr>
                <th>Resim</th>
                <th><asp:FileUpload ID="flResim1" runat="server" /> </th>
                <th><asp:Image ID="imgVirin1" runat="server" /></th>
            </tr>
             <tr><th colspan="3">İkinci Kısım</th></tr>
            <tr>
                <th>Yazı</th>
                <th><asp:TextBox CssClass="yazi-giris" runat="server" ID="txtYazi2"/></th>
                <th></th>
            </tr>
            <tr>
                <th>Resim</th>
                <th><asp:FileUpload ID="flResim2" runat="server" /> </th>
                <th><asp:Image ID="imgVirin2" runat="server" /></th>
            </tr>
            <tr>
                <th></th>
                <th></th>
                <th><asp:Button Text="Güncelle" runat="server" ID="bntGuncelle" OnClick="bntGuncelle_Click" /></th>
            </tr>
        </thead>
    </table>
</asp:Content>
