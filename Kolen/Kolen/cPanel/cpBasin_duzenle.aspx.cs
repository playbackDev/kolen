﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen
{
    public partial class cpBasin_duzenle : cBase
    {
      
            int bID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].IsNumeric())
            {
                bID = Convert.ToInt32(Request.QueryString["id"]);
                if (!Page.IsPostBack)
                {
                    var haber = db.kolen_basin.Where(o => o.id == bID).FirstOrDefault();
                    txtBaslik.Text = haber.baslik;
                    txtKisa.Text = haber.kisa_yazi;
                    txtOzellik.Text = haber.detay;
                }
            }
            else
            {
                Response.Redirect("habers.aspx", false);
            }

        }

        protected void btnHaberDuzenle_Click(object sender, EventArgs e)
        {
            var haber = db.kolen_basin.Where(o => o.id == bID).FirstOrDefault();
            if (txtBaslik.Text != "")
            {
                haber.baslik = txtBaslik.Text;

            }
            if (txtKisa.Text != "")
            {
                haber.kisa_yazi = txtKisa.Text;

            }
            if (txtOzellik.Text != "")
            {
                haber.detay = txtOzellik.Text;
            }

            if (db.SaveChanges() > 0)
            {
                Response.Redirect("cpBasinlar.aspx");
            }

        }
    }
}