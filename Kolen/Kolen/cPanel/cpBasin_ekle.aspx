﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cPanel/cPanel.Master" AutoEventWireup="true" CodeBehind="cpBasin_ekle.aspx.cs" Inherits="Kolen.cPanel.cpBasin_ekle" ValidateRequest="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <h5>Basin Ekle  </h5>
    <asp:Panel runat="server" CssClass="alert alert-error" Visible="false" ID="pnlGeneralError">
        <asp:Literal ID="ltrErrorText" runat="server" />
    </asp:Panel>

    <div class="well">
        <button type="button" class="close" data-dismiss="alert" style="font-size: 16px;">&times;Kapat</button>
         <strong>Not: </strong> Kısa Yazı ve Başlık maximum karakter sayısı 150'tır <br />
    </div>

    <table class="table table-condensed">

        <tr>
            
            <td>
                <asp:TextBox ID="txtBaslik" runat="server" placeholder="Haber Başlık"  MaxLength="150" /></td>
        </tr>
        <tr>
           
            <td>
                <asp:TextBox ID="txtKisa" runat="server" placeholder="Kısa Yazı" MaxLength="150" />
            </td>

        </tr>
       <%-- <tr>
            <td>Anasayfa Resmi</td>
            <td>
                <asp:FileUpload runat="server" ID="fileHaberAnasayfa" />
            </td>

        </tr>--%>
        <tr>
           
            <td>Basın İçerik <br /> 
                <asp:TextBox ID="txtOzellik" runat="server" TextMode="MultiLine"/>
            </td>

        </tr>
        <tr>
            
            <td>
                <asp:Button ID="btnHaberEkle" runat="server" CssClass="btn btn-success" Text="Kaydet" OnClick="btnHaberEkle_Click" /></td>
        </tr>

    </table>
</asp:Content>
