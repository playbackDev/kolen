﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen.cPanel
{
    public partial class cpVitrin :cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var vitrin1 = db.kolen_vitrin.Where(s => s.id == 1).FirstOrDefault();
                var vitrin2 = db.kolen_vitrin.Where(s => s.id == 2).FirstOrDefault();

                txtYazi1.Text = vitrin1.yazi;
                txtYazi2.Text = vitrin2.yazi;

                imgVirin1.ImageUrl = "/uploads/sayfa/" + vitrin1.resim;
                imgVirin2.ImageUrl = "/uploads/sayfa/" + vitrin2.resim;

            }

        }

        protected void bntGuncelle_Click(object sender, EventArgs e)
        {
             var vitrin1 = db.kolen_vitrin.Where(s => s.id == 1).FirstOrDefault();
             var vitrin2 = db.kolen_vitrin.Where(s => s.id == 2).FirstOrDefault();

             if (flResim1.HasFile)
             {
                 string random_name = CreateRandomValue(10, true, true, true, false) + ".jpg";

                 try
                 {
                     flResim1.SaveAs(Server.MapPath("~/uploads/sayfa/" + random_name));
                     ResizeImage("~/uploads/sayfa/" + random_name, 220, 136);
                     vitrin1.resim = random_name;
                 }
                 catch (Exception ex)
                 {
                 }
                 
             }
            vitrin1.yazi = txtYazi1.Text;
            int sayac=0;
            if (db.SaveChanges() > 0)
                sayac++;
            

             if (flResim2.HasFile)
             {
                 string random_name = CreateRandomValue(10, true, true, true, false) + ".jpg";
                 try
                 {
                     flResim1.SaveAs(Server.MapPath("~/uploads/sayfa/" + random_name));
                     ResizeImage("~/uploads/sayfa/" + random_name, 220, 136);
                     vitrin1.resim = random_name;
                 }
                 catch (Exception ex)
                 {

                 }
                 vitrin2.yazi = txtYazi2.Text;
             }
             if (db.SaveChanges() > 0)
                 sayac++;
             if (sayac > 0)
                 RefreshPage();
        }
    }
}