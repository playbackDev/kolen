﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen.cPanel
{
    public partial class cpSayfa_duzenle : cBase
    {
        int sID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Request.QueryString["id"].IsTextBoxEmptyOrNull() && Request.QueryString["id"].IsNumeric())
            {
                sID = Convert.ToInt32(Request.QueryString["id"]);
                if (!Page.IsPostBack)
                {
                    var sayfa = db.kolen_sayfalar.Where(o => o.id == sID).FirstOrDefault();
                    txtBaslik.Text = sayfa.baslik;
                    txtOzellik.Text = sayfa.icerik;
                }
            }
            else
            {
                Response.Redirect("cpSayfalar.aspx", false);
            }
        }

        protected void btnHaberDuzenle_Click(object sender, EventArgs e)
        {
            var sayfa = db.kolen_sayfalar.Where(o => o.id == sID).FirstOrDefault();
            if (txtBaslik.Text != "")
            {
                sayfa.baslik = txtBaslik.Text;

            }
           
            if (txtOzellik.Text != "")
            {
                sayfa.icerik = txtOzellik.Text;
            }

            if (db.SaveChanges() > 0)
            {
                Response.Redirect("cpSayfalar.aspx");
            }

        }

      
    }
}