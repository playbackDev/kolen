﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cPanel/cPanel.Master" AutoEventWireup="true" CodeBehind="cpModal_duzenle.aspx.cs" Inherits="Kolen.cPanel.cpModal_duzenle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h5>Modal Düzenle</h5>
    <div class="well">
        <button type="button" class="close" data-dismiss="alert" style="font-size:16px;">&times;Kapat</button>
        <strong>Dikkat!</strong> Slider resimlerini 700x400 px veya aynı orantıda yükleyin. <br />
</div>
     <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th><asp:Image ID="imgModal" runat="server" /></th>
                    
                </tr>
                <tr>
                    <th style="text-align: left; vertical-align: middle; width: 10%;"> <asp:CheckBox Text="Aktif" runat="server" ID="chkAktif" /> </th>
                </tr>
                <tr>
                   <th style="text-align: left; vertical-align: middle; width: 10%;"> <asp:FileUpload ID="flArsivSlide" runat="server" /> <asp:Button ID="btnAnaSlide" Text="Yükle" runat="server" CssClass="btn btn-success" OnClick="btnAnaSlide_Click" /> </th>

                </tr>
            </thead>
         </table>
</asp:Content>
