﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen.cPanel
{
    public partial class haber_ekle : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnHaberEkle_Click(object sender, EventArgs e)
        {
            kolen_news yeniHaber = new kolen_news();
            if (txtBaslik.Text!="")
            {
                pnlGeneralError.Visible = false;
                yeniHaber.baslik = txtBaslik.Text;
                if (txtKisa.Text!="")
                {
                    yeniHaber.kisa_yazi = txtKisa.Text;
                    pnlGeneralError.Visible = false;
                    if (txtOzellik.Text!="")
                    {
                        yeniHaber.detay = txtOzellik.Text;
                        yeniHaber.is_active = false;
                        yeniHaber.create_date = DateTime.Now;

                        if (fileHaberAnasayfa.HasFile)
                        {
                            string random_name = CreateRandomValue(10, true, true, true, false) + ".jpg";

                            try
                            {
                                fileHaberAnasayfa.SaveAs(Server.MapPath("~/uploads/haber/" + random_name));
                                ResizeImage("~/uploads/haber/" + random_name, 220, 200);
                                yeniHaber.resim = random_name;
                            }
                            catch (Exception ex)
                            {
                            }

                        }

                        db.kolen_news.Add(yeniHaber);
                        if (db.SaveChanges()>0)
                        {
                            Response.Redirect("habers.aspx");
                        }
                    }
                    else
                    {
                        ltrErrorText.Text = "Lütfen haber detayını girin.";
                        pnlGeneralError.Visible = true;
                    }                   
                }
                else
                {
                    ltrErrorText.Text = "Lütfen bir kısa yazı girin.";
                    pnlGeneralError.Visible = true;
                }
            }
            else
            {
                ltrErrorText.Text = "Lütfen bir haber başlığı girin.";
                pnlGeneralError.Visible = true;
            }
        }
    }
}