﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Kolen.cPanel.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
   <meta charset="utf-8">
    <title>Yönetim Paneli</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="http://www.playback.com.tr">

    <!-- Le styles -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <link href="/assets/css/ddsmoothmenu.css" rel="stylesheet">
    <link href="/assets/css/panel.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le javascript
    ================================================== -->
    <script src="/assets/js/jquery-1.7.2.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/panel.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand" href="#"></a>
        </div>
    </div>
</div>
        <br /><br /><br />
<div class="container">
    <div class="row">
        <div class="span2 offset3">
             
            <img src="/assets/img/decrypted.png" /> 
        </div>
        <div class="span3">
            <section>
                <table class="table table-bordered">
                    <tr>
                        <th>Kolen Yönetim Paneli </th>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="txtUser" runat="server" placeholder="Kullanıcı Adı"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="txtPass" runat="server" TextMode="Password" placeholder="Şifreniz"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td><asp:Button ID="btnLogin" runat="server" Text="Giriş" onclick="btnLogin_Click" CssClass="btn btn-success" /></td>
                    </tr>
                </table>
            </section>
        </div>
    </div>
</div>

<div class="container">
    <footer>
        <p>&copy;2013 <br /></p>
    </footer>
</div>
    </form>
</body>
</html>
