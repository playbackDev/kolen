﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cPanel/cPanel.Master" AutoEventWireup="true" CodeBehind="cpBilgiler.aspx.cs" Inherits="Kolen.cPanel.cpBilgiler" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        var acik = 0;
        $(document).ready(function () {

            $(".orderClick").click(function () {
                var csi = $(this).attr("c");
                if (acik != csi) {
                    $(".tler").hide();
                    $(this).parent().parent().parent().next().show();
                    acik = csi;
                }
                else {
                    $(this).parent().parent().parent().next().hide();
                    acik = 0;
                }
            });
        });
    </script>
    <style>
        .ortala td, tr,th {
            text-align: center;
            vertical-align:middle;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <h5>Sayfalar</h5>
    <%--<div class="well">
        <button type="button" class="close" data-dismiss="alert" style="font-size: 16px;">&times;Kapat</button>
         <strong>Not: </strong>Maden Liste Resmi 220x166 ebatlarındadır. Bu ebatlarla orantılı resim yükleyebilirsiniz. <br />
        <span style="text-decoration:underline;font-style:italic;">Sistem otomatik boyutlandıracaktır.</span>
    </div>--%>
     <asp:Panel runat="server" CssClass="alert alert-error" Visible="false" ID="pnlGeneralError">
        <asp:Literal ID="ltrErrorText" runat="server" />
    </asp:Panel>
    <asp:Literal ID="ltrss" Text="" runat="server" />
    <table class="table table-bordered ortala">
        <thead>
            <tr>
                <th style="text-align: center;">ID</th>
                <th style="text-align: center;">Bilgi Başlık</th>
                <th style="text-align: center;">Bilgi İşlem</th>
            </tr>
        </thead>
        <tbody>
            <asp:Repeater ID="rptHaberler" runat="server" OnItemDataBound="rptHaberler_ItemDataBound">
                <ItemTemplate>
                    <tr>
                        <td><%#Eval("id") %></td>
                        <td><%#Eval("baslik") %></td>
                        <td>
                            <div class="btn-group">
                                <a href="javascript:void(0)" class="orderClick btn btn-primary" c='<%#Eval("id") %>'>İncele</a>
                                <asp:LinkButton ID="lnkAktif" Text="" runat="server" CommandArgument='<%#Eval("id") %>' OnCommand="lnkAktif_Command"/>
                                <a href="cpBilgi_duzenle.aspx?id=<%#Eval("id") %>" class="btn">Düzenle</a>
                            </div>
                        </td>
                    </tr>
                    <tr style="display: none;" class="tler">
                        <td colspan="4">
                            <div class="alert alert-success" style="margin-bottom: 0px; text-align: left; line-height: 25px;">
                                
                                <strong>Kısa Yazı</strong>
                                <div style="text-indent: 15px;"><%#Eval("kisa_yazi") %></div>
                               <strong> Detay Yazısı</strong>
                                <div style="text-indent: 15px;"><%#Eval("detay") %></div>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </tbody>
    </table>
</asp:Content>
