﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen.cPanel
{
    public partial class login : cBase
    {
        cMember cm = new cMember();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (cm.adminIsLogged())
                Response.Redirect("default.aspx");

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtUser.Text != "" && txtPass.Text != "")
            {
                if (cm.adminLogin(txtUser.Text, txtPass.Text))
                {
                    string cookie_content = cEncrytion.EncryptString(txtUser.Text + "-ls-" + txtPass.Text);
                    Response.Cookies["kol"].Value = cookie_content;
                    Response.Cookies["kol"].Expires = DateTime.Now.AddDays(7);
                    Response.Redirect("default.aspx");
                }
            }
        }
    }
}