﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Kolen.Master" AutoEventWireup="true" CodeBehind="haberler.aspx.cs" Inherits="Kolen.haberler" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="page-baslik">
                    HABERLER
                </div>
                <div class="page-baslik mt10 page-icerik">

                    <asp:Repeater runat="server" ID="rptHaberler">
                        <ItemTemplate>
                            <a href="/haber.aspx?id=<%#Eval("id")%>">
                                <div class="haber_cont">
                                    <div class="row">
                                        <div class="span3">
                                            <img src="/uploads/haber/<%#Eval("resim")%>" />
                                        </div>
                                        <div class="span7">
                                            <p><%#Eval("baslik")%></p>
                                            <p><%#Eval("kisa_yazi")%></p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </ItemTemplate>
                    </asp:Repeater>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
