﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Kolen.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Kolen.main" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/assets/css/bjqs.css" rel="stylesheet" />
    <link href="/assets/css/demo.css" rel="stylesheet" />
    <script class="secret-source" type="text/javascript">
        jQuery(document).ready(function ($) {
            $('#banner-fade').bjqs({
                height: 304,
                width: 940,
                responsive: false
            });
        });
    </script>
    <script src="assets/js/bjqs-1.3.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="mt25 mb25" style="width: 100%; height: 304px; background-image: url(/assets/img/sliderbg.png)">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <div id="slider">

                        <div id="banner-fade">
                            <ul class="bjqs">
                                <asp:Repeater ID="rptBanner1" runat="server">
                                    <ItemTemplate>
                                        <li>
                                            <a href="<%#Eval("link")%>">
                                                <div class="banner-yazi">
                                                    <p class="banner-baslik"><%#Eval("baslik")%></p>
                                                    <p class="banner-icerik"><%#Eval("adi")%></p>
                                                </div>
                                            </a>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="span3 ml0">
                    <a href="/haberler.aspx" title="Haberler">
                        <img src="/assets/img/haberler.png" title="Haberler" alt="Haberler" /></a>
                    <div class="row">
                        <div class="span3 mt10 infoBGs">
                            <asp:Repeater ID="rptHaberler" runat="server" OnItemDataBound="rptHaberler_ItemDataBound">
                                <ItemTemplate>
                                    <img src="assets/img/newico.jpg" />
                                    <a class="newsa" href="/haber.aspx?id=<%#Eval("id") %>"><%#Eval("baslik") %></a>
                                    <br />
                                    <%#Eval("kisa_yazi") %>
                                    <br />
                                    <asp:Literal ID="ltrTarih" Text="" runat="server" />
                                    <br />
                                    <br />

                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
                <div class="span3">
                    <a href="/faq.aspx" title="Bilgiler">
                        <img src="/assets/img/bilgiler.png" title="Bilgiler" alt="Bilgiler" /></a>
                    <div class="row">
                        <div class="span3 mt10 infoBGs">
                            <asp:Repeater ID="rptBilgiler" runat="server">
                                <ItemTemplate>
                                    <img src="assets/img/newico.jpg" />
                                    <a class="newsa" href="/faq.aspx?id=<%#Eval("id") %>"><%#Eval("baslik") %></a>
                                    <br />
                                    <br />
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
                <div class="span3">
                    <a href="javascript:void(0);" onclick="IndirimTablosu(1)" title="Fiyat Kıyaslaması">
                        <img src="/assets/img/fiyat-kisitlamasi.png" title="Fiyat Kıyaslaması" alt="Fiyat Kıyaslaması" /></a>
                    <div class="row">
                        <div class="span3 infoBGs noBg alignn" style="padding-left: 0px;">
                            <div class="vitrin-yazi">
                                <p class="newsa">
                                    <asp:Literal ID="ltrYazi1" runat="server" /></p>

                            </div>

                            <%--<img class="mt5" src="assets/img/kiyas.png" />--%>
                            <asp:Image ID="imgKisim1" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="span3">
                    <a href="/iletisim.aspx" title="Müşterimiz Olmak İçin">
                        <img src="/assets/img/musteriolmakicin.png" title="Müşterimiz Olmak İçin" alt="Müşterimiz Olmak İçin" /></a>
                    <div class="row">
                        <div class="span3 infoBGs noBg alignn" style="padding-left: 0px;">
                            <div class="vitrin-yazi">
                                <p class="newsa">
                                    <asp:Literal ID="ltrYazi2" runat="server" />
                            </div>

                            <%--<img class="mt5" src="assets/img/musteri_ol.jpg" />--%>
                            <asp:Image ID="imgKisim2" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
