﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen
{
    public partial class page : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int sayfaId;
            leftmenuKurumsal.katId = 1;
            if (!Request.QueryString["id"].IsTextBoxEmptyOrNull()&&Request.QueryString["id"].IsNumeric())
            {
                sayfaId = Convert.ToInt16(Request.QueryString["id"]);
                try
                {
                    var sayfa = db.kolen_sayfalar.Where(s => s.id == sayfaId).FirstOrDefault();
                    ltrBaslik.Text = sayfa.baslik;
                    ltrIcerik.Text = sayfa.icerik;
                    leftmenuKurumsal.katId = sayfa.kategoriId;
                    leftmenuKurumsal.pageId = sayfa.id;
                }
                catch
                { 
                
                
                }
            
            }
        }
    }
}