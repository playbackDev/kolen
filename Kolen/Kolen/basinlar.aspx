﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Kolen.Master" AutoEventWireup="true" CodeBehind="basinlar.aspx.cs" Inherits="Kolen.basinlar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row">
             <div class="span12">
                <div class="page-baslik">
                    MEDYA
                </div>
                <div class="page-baslik mt10 page-icerik">
                     <asp:Repeater runat="server" ID="rptBasinlar">
                        <ItemTemplate> 
                            <a href="/basin.aspx?id=<%#Eval("id")%>">
                            <div class="haber_cont">
                                <p><%#Eval("baslik")%></p>
                                <p><%#Eval("kisa_yazi")%></p>
                            </div>
                            </a>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
