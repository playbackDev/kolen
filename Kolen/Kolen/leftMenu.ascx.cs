﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen
{
    public partial class leftMenu : System.Web.UI.UserControl
    {
        public int katId = 0;
        public int pageId=0;

        sitelerEntities db = new sitelerEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (katId != 0)
            {
                var sayfalar = db.kolen_sayfalar.Where(s => s.kategoriId == katId).ToList();
                rptSayfalar.DataSource = sayfalar;
                rptSayfalar.DataBind();
            }
        }
    }
}