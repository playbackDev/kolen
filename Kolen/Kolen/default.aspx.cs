﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen
{
    public partial class main : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                rptHaberler.BindVeri(db.kolen_news.Where(o=>o.is_active==true).Take(2).ToList());
                rptBilgiler.BindVeri(db.kolen_bilgiler.Where(o => o.is_active == true).Take(4).ToList());
                rptBanner1.BindVeri(db.kolen_banner.OrderBy(s => s.sira).ToList());

                var vitrin1 = db.kolen_vitrin.Where(s => s.id == 1).FirstOrDefault();
                var vitrin2 = db.kolen_vitrin.Where(s => s.id == 2).FirstOrDefault();

                ltrYazi1.Text = vitrin1.yazi;
                ltrYazi2.Text = vitrin2.yazi;

                imgKisim1.ImageUrl = "/uploads/sayfa/" + vitrin1.resim;
                imgKisim2.ImageUrl = "/uploads/sayfa/" + vitrin2.resim;
            }
         
        }

        protected void rptHaberler_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Literal ltr = (Literal)e.Item.FindControl("ltrTarih");
            DateTime tarih =Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem,"create_date"));
            ltr.Text = tarih.Day + "/" + tarih.Month + "/" + tarih.Year;
        }
    }
}