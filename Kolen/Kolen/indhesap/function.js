﻿var BGArray = ["bg-1.jpg", "bg-2.jpg", "bg-3.jpg", "bg-4.jpg", "bg-meslek2.jpg"]
//var BGArray1 = ["bg1.jpg", "bg2.jpg", "bg3.jpg", "bg4.jpg", "bg5.jpg"]
var MenuArray = ["hakkimizda", "elektrik-satisi", "elektrik-uretimi", "elektrik-dagitim", "referanslarimiz", "bilgi-merkezi", "haberler", "bize-ulasin"];
var ContentArray0 = ["kazanci-holding", "kurum-kulturu", "aksa-toptan-satis"];
var ContentArray1 = ["elektrik-satisi-nedir", "neden-aksa-elektrik", "sss"]
var ContentArray2 = ["elektrik-uretim-santralleri", "projeler"]
var ContentArray3 = ["coruh-edas", "firat-edas"]
var ContentArray4 = ["referanslarimiz"]
var ContentArray5 = ["serbest-tuketiciler", "elektrik-piyasasi-islemleri"]
var ContentArray6 = ["aksadan-haberler", "basinda-aksa", "medya"]
var ContentArray7 = ["satis-noktalari", "iletisim-bilgileri", "teklif-istek-formu", "insan-kaynaklari"]

shuffle = function(v){
    for(var j, x, i = v.length; i; j = parseInt(Math.random() * i), x = v[--i], v[i] = v[j], v[j] = x);
    return v;
};

//var BGArray = shuffle(BGArray1);

function Aksa(id, title) {
    $('.firma').css('display', 'none');
    $('.content-title').text(title);
    $('#' + id).fadeIn('slow');
}
function ShowSubmenu(id) {
    $('#submenu').fadeIn().dequeue().animate({ "left": "170px" }, "slow", function() {
        $('#submenu-' + id).show();
        submenuShow = 1;
		if (id == 'referanslarimiz') {
			$('#reff').trigger("click");
		}
    });
    $('.cont').empty().hide();
    activeSubmenu = id;
}
function HideSubmenu() {
    $('#submenu').fadeOut().dequeue().animate({ "left": "0" }, "slow", function() {
        submenuShow = 0;
        $('.sub').hide();
        activeSubmenu = '';
    });
}
function ShowContent(id) {
    $('.cont').empty().hide();
    $('#content').fadeIn().dequeue().animate({ "left": "341px" }, "slow", function() {
        $('.content-title').text(ContentTitle);
        contentShow = 1;
		if (MainPage == 'referanslarimiz') {
			LoadContent('referanslarimiz');
		} else {
			LoadContent(id);
		}
    });
}
function HideContent() {
    $('.cont').empty().hide();
    $('#content').fadeOut().dequeue().animate({ "left": "0" }, "slow");
    contentShow = 0;
    activeContent = '';
    ContentTitle = '';
    if ($('#holding-menu').is(":visible") == true) {
        $('#holding-menu').hide();
    }
}
function LoadContent(id) {
    if (id == 'elektrik-uretim-santralleri') {
        $('.cont').hide();
        $('.cont2').show();
        $('#holding-menu').hide();
        $('.cont2').load('content/' + id + '.aspx').show();
    } else {
        $('.cont2').empty().hide();
        $('.cont').show();
        $('.cont').empty().html('<img src="img/loading.gif" alt="" title="">');
        $('.cont').height('357px').width('793px').css("overflow", "auto");
        if (id == 'kazanci-holding') {
            $('.cont').height('300px');
            $('#holding-menu').fadeIn();
        } else {
            $('#holding-menu').hide();
        }
        $('.cont').load('content/' + id + '.aspx').show();
    }
}
function ResizeWindow() {
    WindowHeight = $(window).height();
    WindowWidth = $(window).width();
    if (WindowHeight > 768) {
        MarginTop = (WindowHeight - ContentHeight) / 2
    } else {
        MarginTop = 35;
    }
    $('#wrapper').css("top", MarginTop);
    $('#bgchange').css("top", (WindowHeight - 67) / 2);
    $('#shadow').css("top", -1 * MarginTop);
}
function ChangeBG() {
    if (activeBG < BGCount) {
        NextBG = activeBG + 1;
    } else {
        NextBG = 1
    }
    activeBG = NextBG;
    $('#background').fadeOut('slow').queue(function() {
        $(this).attr("src", 'img/' + BGArray[activeBG - 1]).fadeIn();
        $(this).dequeue();
    });
}
function StopInterval() {
    window.clearInterval(bgint);
}
function ChangeMap(id) {
    $('#harita-' + id).siblings().hide();
    $('#harita-' + id + ' a').show();
    $('#harita-' + id).show();
}
function ShowIcon(icon, id) {
    ChangeMap(id);
    $('#harita-' + id + ' a').hide();
    $('#harita-' + id + ' .icon-' + icon).show();
}
function QA(id) {
    $('.answer').slideUp('fast');
    $('#answer' + id).slideToggle('fast');
}
function HaberDetay(id, tur) {
    $('#haber-content').html('<img src="img/loading.gif" alt="" title="">');
    $.ajax({
        type: "GET",
        cache: false,
        dataType: "html",
        url: 'haberler/' + tur + '-' + id + '.aspx',
        success: function(data) {
            $('#haber-content').empty().delay(500).html(data);
        }
    });
}
var SiteLink = encodeURIComponent("http://www.aksaelektrik.com.tr/Default.aspx");
function Facebook() {
    window.open('http://www.facebook.com/sharer.php?u=' + SiteLink + '&t=800 TL ve üzeri elektrik faturası ödeyenler için İNDİRİMLİ ELEKTRİK', 'mywindow', 'toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,copyhistory=yes,resizable=yes');
}
function Twitter() {
    window.open('https://twitter.com/share?text=800+TL+ve+%C3%BCzeri+elektrik+faturas%C4%B1+%C3%B6deyenler+i%C3%A7in+%C4%B0ND%C4%B0R%C4%B0ML%C4%B0+ELEKTR%C4%B0K&url=' + SiteLink, 'mywindow', 'toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,copyhistory=yes,resizable=yes');
}
$(function() {
    $('#menu ul li a').click(function() {
        $('.sub ul li').css('padding', '0').removeClass("current");
        $('.sub ul li a').removeClass("current");
        if (submenuShow == 0) {
            activeSubmenu = MainPage;
            ShowSubmenu(activeSubmenu);
            $(this).parent("li").addClass("current").siblings().removeClass("current");
            $('#menu ul li a').removeClass("current")
            $(this).addClass("current");
        } else {
            if (activeSubmenu != $(this).attr("href").replace('#/', '')) {
                $('#submenu-' + activeSubmenu).hide();
                activeSubmenu = $(this).attr("href").replace('#/', '');
                $('#submenu-' + activeSubmenu).show();
                $(this).parent("li").addClass("current").siblings().removeClass("current");
                $('#menu ul li a').removeClass("current")
                $(this).addClass("current");
				if (activeSubmenu == 'referanslarimiz') {
					$('#reff').trigger("click");
				}
            } else {
                //				HideSubmenu();
                //				$(this).parent("li").removeClass("current");
                //				$(this).removeClass("current")
            }
        }
    });
    $('#submenu ul li a').click(function() {
        if (contentShow == 0) {
            activeContent = SubPage;
            ContentTitle = $(this).text();
            if (SubPage == 'elektrik-piyasasi-islemleri')
                ContentTitle = 'ELEKTRİK PİYASASI İŞLEMLERİ';
            $(this).parent("li").siblings().animate({ paddingTop: '0px', paddingBottom: '0px' }).removeClass("current");
            $('#submenu ul li a').removeClass("current");
            $(this).parent("li").addClass("current").animate({ paddingTop: '17px', paddingBottom: '17px' });
            $(this).addClass("current");
            ShowContent(activeContent);
        } else {
            if (activeContent != SubPage) {
                activeContent = SubPage;
                ContentTitle = $(this).text();
                if (SubPage == 'elektrik-piyasasi-islemleri')
                    ContentTitle = 'ELEKTRİK PİYASASI İŞLEMLERİ';
                $('.content-title').text(ContentTitle);
                $(this).parent("li").siblings().animate({ paddingTop: '0px', paddingBottom: '0px' }).removeClass("current");
                $('#submenu ul li a').removeClass("current");
                $(this).parent("li").addClass("current").animate({ paddingTop: '17px', paddingBottom: '17px' });
                $(this).addClass("current");
                LoadContent(activeContent);
            }
        }
    });
    $(window).resize(function() { ResizeWindow() });
    $('#holding-menu a').hover(
		function() {
		    $(this).find('.img').animate({ height: 75, marginTop: 15, opacity: 0.5 }, 200);
		    $(this).find('.txt').fadeIn();
		},
		function() {
		    $(this).find('.txt').fadeOut();
		    $(this).find('.img').animate({ height: 45, marginTop: 45, opacity: 1 }, 200);
		}
	);
    $("#haber-listesi ul li a").live("click", function() {
        $('#haber-listesi ul li a').removeClass("current");
        $(this).addClass("current");
    });
    $('#HaberDown').live({
        mouseover: function() {
            var y = $('#haber-listesi ul').height() - $('#haber-listesi #list').height();
            $('#haber-listesi ul').animate({ marginTop: '-' + y }, 1500);
        },
        mouseout: function() {
            $('#haber-listesi ul').stop();
        }
    });
    $('#HaberUp').live({
        mouseover: function() {
            $('#haber-listesi ul').animate({ marginTop: 0 }, 1500);
        },
        mouseout: function() {
            $('#haber-listesi ul').stop();
        }
    });
});
var MainPage = '';
var SubPage = '';
function isNumber(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function Git(id) {
    document.location.href = id;
}
function FaturaEkle(id) {
    $('#f' + id).show();
    $('#fe' + id).hide();
}








