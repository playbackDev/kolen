﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen.indhesap
{
    public partial class indirim4 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var mesken_kolen = db.kolen_indirimlihesap.Where(s => s.firma == "KOLEN" && s.tip == "MESKEN").FirstOrDefault();
            var mesken_tedas = db.kolen_indirimlihesap.Where(s => s.firma == "TEDAŞ" && s.tip == "MESKEN").FirstOrDefault();
            var sabitler = db.kolen_indhesap_sabitler.FirstOrDefault();

            A1.Text = mesken_tedas.per_sat_bed.ToString();
            A3.Text = mesken_kolen.per_sat_bed.ToString();

            B1.Text = mesken_tedas.dagitim.ToString();
            B3.Text = mesken_kolen.dagitim.ToString();

            C1.Text = mesken_tedas.iletim.ToString();
            C3.Text = mesken_kolen.iletim.ToString();

            D1.Text = mesken_tedas.psh.ToString();
            D3.Text = mesken_kolen.psh.ToString();

            F1.Text = mesken_tedas.kayip.ToString();
            F3.Text = mesken_kolen.kayip.ToString();

            TRTPayi.Text = sabitler.trtpayi;
            EnerjiFonu.Text = sabitler.enerjifonu;
            BTV.Text = sabitler.btv;


        }
    }
}