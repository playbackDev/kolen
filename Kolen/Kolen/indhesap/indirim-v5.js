﻿function ShowIndirimHesapla() {
    if (submenuShow == 1) {
        HideSubmenu();
        $('#menu ul li.current').removeClass("current").children().removeClass("current");
    }
    if (contentShow == 1) {
        HideContent();
    }
    if ($('#indirimmenu').is(":visible") == false) {
        $('#indirimmenu').fadeIn().dequeue().animate({
            left: '387px',
            width: '755px'
        }, 1000);
    }
}
function HideIndirimHesapla() {
    $('#indirimmenu').fadeOut().dequeue().animate({
        left: '954px',
        width: '0px'
    }, 1000);
    HideIndirimDiv();
}
function HideIndirimDiv() {
    if ($('#indirimdiv').is(":visible") == true) {
        $('.indirim-cont').fadeOut('fast');
        $('#indirimdiv').fadeOut().dequeue().animate({
            top: '57px',
            height: '0px'
        }, 1000);
        $('#indirimmenu ul li a').removeClass("current");
    }
}
function IndirimTablosu(id) {
   
    $('#indirimmenu ul li a').removeClass("current");
    $('#indirimmenu ul .menu' + id + ' a').addClass("current");
    if ($('#indirimdiv').is(":visible") == false) {
        $('#indirimdiv').fadeIn().dequeue().animate({
            top: '150px',
            height: '730px'
        }, 1000, function() {
            LoadIndirim(id);
        });
    } else {
        LoadIndirim(id);
    }
}
function LoadIndirim(id) {
   
    $('.indirim-cont').html('<img src="assets/img/loading.gif" alt="" title="">');
    $.ajax({
        type: "GET",
        cache: false,
        dataType: "html",
        url: '/indhesap/indirim' + id + '.aspx',
        success: function (data) {
            $('.indirim-cont').html(data).fadeIn('fast');
            var d = new Date();
            var gun = d.getDate();
            if (gun < 10) { gun = "0" + gun; }
            var ay = d.getMonth();
            if (ay < 10) { ay = "0" + ay; }
            $('#FaturaTarihi').val("Tarih: " + gun + "." + ay + "." + d.getFullYear());
        },error: function () {
        
            alert("istek başarısız");
        }
    });
}
function Hesapla(id) {
    $('#isim').val($('#AdSoyad').val());
    var TuketimMiktari = $('#TuketimMiktari').val();
    var IndirimOrani = $('#IndirimOrani').val();
    if (TuketimMiktari > 0) {
		var L1 = TuketimMiktari;
		$('#L1').val(TuketimMiktari+".00 TL");
    }
    if (IndirimOrani > 0 && TuketimMiktari > 0) {
        if (TuketimMiktari < 150) {
            alert('İndirim hesaplamak için aylık ortalama tüketiminiz minimum 150 TL olmalıdır.');
            return false;
        }
		/* Birinci Sütun */
		var A1 = $('#A1').val();
		var B1 = $('#B1').val();
		var C1 = $('#C1').val();
		var D1 = $('#D1').val();
		var F1 = $('#F1').val();
		var EnerjiFonu = ($('#EnerjiFonu').val()) / 100
        if (id == 4) {
            var G1 = (parseFloat(parseFloat(A1) + parseFloat(F1)) * EnerjiFonu);
        } else {
            var G1 = (parseFloat(parseFloat(A1) + parseFloat(F1)) * EnerjiFonu);
        }
        $('#G1').val(G1.toFixed(3));
		var TRTPayi = ($('#TRTPayi').val()) / 100
        if (id == 4) {
            var H1 = (parseFloat(parseFloat(A1) + parseFloat(F1)) * TRTPayi);
        } else {
            var H1 = (parseFloat(parseFloat(A1) + parseFloat(F1)) * TRTPayi);
        }
		$('#H1').val(H1.toFixed(3));
		var BTV = ($('#BTV').val()) / 100
        if (id == 4) {
            var I1 = (parseFloat(parseFloat(A1) + parseFloat(F1)) * BTV);
        } else {
            var I1 = (parseFloat(parseFloat(A1) + parseFloat(F1)) * BTV);
        }
        $('#I1').val(I1.toFixed(3));
		var J1 = parseFloat(parseFloat(A1) + parseFloat(B1) + parseFloat(C1) + parseFloat(D1) + parseFloat(F1) + parseFloat(G1) + parseFloat(H1) + parseFloat(I1));
        $('#J1').val(J1.toFixed(3));
		var K1 = J1 * 0.18
        $('#K1').val(K1.toFixed(3));
		var L2 = J1 + K1;
        $('#L2').val(L2.toFixed(3)/*.replace(".", ",")*/ + " TL.");
		
		/*** İkinci Sütun ***/
		var AylikKWH = (L1/L2*100).toFixed(2);
		$('.Aylikkwh').text(AylikKWH)
		var AylikKWH2 = AylikKWH;
		$('.Aylikkwh2').text(Math.round(AylikKWH2));
		
        var A2 = parseFloat((A1 * AylikKWH) / 100);
        $('#A2').val(A2.toFixed(3));
        var B2 = parseFloat((B1 * AylikKWH) / 100).toFixed(3);
        $('#B2').val(B2);
        var C2 = parseFloat((C1 * AylikKWH) / 100).toFixed(3);
        $('#C2').val(C2);
        var D2 = parseFloat((D1 * AylikKWH) / 100).toFixed(3);
        $('#D2').val(D2);
        var F2 = parseFloat((F1 * AylikKWH) / 100).toFixed(3);
        $('#F2').val(F2);       
        var G2 = parseFloat((G1 * AylikKWH) / 100);
        $('#G2').val(G2.toFixed(3));
        var H2 = parseFloat((H1 * AylikKWH) / 100);
        $('#H2').val(H2.toFixed(3));
        var I2 = parseFloat((I1 * AylikKWH) / 100);
        $('#I2').val(I2.toFixed(3));
        var J2 = parseFloat(parseFloat(A2) + parseFloat(B2) + parseFloat(C2) + parseFloat(D2) + parseFloat(F2) + parseFloat(G2) + parseFloat(H2) + parseFloat(I2));
        $('#J2').val(J2.toFixed(3));
        var K2 = J2 * 0.18
        $('#K2').val(K2.toFixed(3));
		

		/*** Üçüncü Sütun  ***/
        var A3 = parseFloat(A1 * ((100 - IndirimOrani) / 100));
        $('#A3').val(A3.toFixed(3));
		var B3 = $('#B3').val();
		var C3 = $('#C3').val();
		var D3 = $('#D3').val();
		var F3 = $('#F3').val();
        var G3 = (parseFloat(parseFloat(A3) + parseFloat(F3)) * EnerjiFonu);
        $('#G3').val(G3.toFixed(3));		
        var H3 = (parseFloat(parseFloat(A3) + parseFloat(F3)) * TRTPayi);
        $('#H3').val(H3.toFixed(3));
        var I3 = (parseFloat(parseFloat(A3) + parseFloat(F3)) * BTV);
        $('#I3').val(I3.toFixed(3));
        var J3 = parseFloat(parseFloat(A3) + parseFloat(B3) + parseFloat(C3) + parseFloat(D3) + parseFloat(F3) + parseFloat(G3) + parseFloat(H3) + parseFloat(I3));
        $('#J3').val(J3.toFixed(3));
        var K3 = J3 * 0.18
        $('#K3').val(K3.toFixed(3));
        var L3 = J3 + K3;
        $('#L3').val(L3.toFixed(3)/*.replace(".", ",") */ + " TL.");
		
		
		/*** Dördüncü Sütun ***/
        var A4 = parseFloat((A3 * AylikKWH2) / 100);
        $('#A4').val(A4.toFixed(3));
        var B4 = parseFloat((B3 * AylikKWH2) / 100).toFixed(3);
        $('#B4').val(B4);
        var C4 = parseFloat((C3 * AylikKWH2) / 100).toFixed(3);
        $('#C4').val(C4);
        var D4 = parseFloat((D3 * AylikKWH2) / 100).toFixed(3);
        $('#D4').val(D4);
        var F4 = parseFloat((F3 * AylikKWH2) / 100).toFixed(3);
        $('#F4').val(F4);
        var G4 = parseFloat(AylikKWH2 * G3 * 0.01);
        $('#G4').val(G4.toFixed(3));
        var H4 = parseFloat(AylikKWH2 * H3 * 0.01);
        $('#H4').val(H4.toFixed(3));
        var I4 = parseFloat(AylikKWH2 * I3 * 0.01);
        $('#I4').val(I4.toFixed(3));
        var J4 = parseFloat(parseFloat(A4) + parseFloat(B4) + parseFloat(C4) + parseFloat(D4) + parseFloat(F4) + parseFloat(G4) + parseFloat(H4) + parseFloat(I4));
        $('#J4').val(J4.toFixed(3));
        var K4 = J4 * 0.18
        $('#K4').val(K4.toFixed(3));
        var L4 = J4 + K4;
        $('#L4').val(L4.toFixed(3)/*.replace(".", ",") */ + " TL.");

        var KazancAylik = (TuketimMiktari - L4);
        //$('#KazancAylik').val(Math.round(KazancAylik))
        var KazancYillik = Math.round(KazancAylik * 12);
        $('#KazancYillik').val(KazancYillik + " TL.")
    }
}