﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen.indhesap
{
    public partial class indirim1:cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var ticarethane_kolen = db.kolen_indirimlihesap.Where(s => s.firma == "KOLEN" && s.tip == "TİCARETHANE").FirstOrDefault();
            var ticarethane_tedas = db.kolen_indirimlihesap.Where(s => s.firma == "TEDAŞ" && s.tip == "TİCARETHANE").FirstOrDefault();
            var sabitler = db.kolen_indhesap_sabitler.FirstOrDefault();
            
            A1.Text = ticarethane_tedas.per_sat_bed.ToString();
            A3.Text = ticarethane_kolen.per_sat_bed.ToString();

            B1.Text = ticarethane_tedas.dagitim.ToString();
            B3.Text = ticarethane_kolen.dagitim.ToString();

            C1.Text = ticarethane_tedas.iletim.ToString();
            C3.Text = ticarethane_kolen.iletim.ToString();

            D1.Text = ticarethane_tedas.psh.ToString();
            D3.Text = ticarethane_kolen.psh.ToString();

            F1.Text = ticarethane_tedas.kayip.ToString();
            F3.Text = ticarethane_kolen.kayip.ToString();

            TRTPayi.Text = sabitler.trtpayi;
            EnerjiFonu.Text = sabitler.enerjifonu;
            BTV.Text = sabitler.btv;

        }
    }
}