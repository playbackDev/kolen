﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kolen.indhesap
{
    public partial class indirim2 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var sanayi_kolen = db.kolen_indirimlihesap.Where(s => s.firma == "KOLEN" && s.tip == "SANAYİ").FirstOrDefault();
            var sanayi_tedas = db.kolen_indirimlihesap.Where(s => s.firma == "TEDAŞ" && s.tip == "SANAYİ").FirstOrDefault();
            var sabitler = db.kolen_indhesap_sabitler.FirstOrDefault();

            A1.Text = sanayi_tedas.per_sat_bed.ToString();
            A3.Text = sanayi_kolen.per_sat_bed.ToString();

            B1.Text = sanayi_tedas.dagitim.ToString();
            B3.Text = sanayi_kolen.dagitim.ToString();

            C1.Text = sanayi_tedas.iletim.ToString();
            C3.Text = sanayi_kolen.iletim.ToString();

            D1.Text = sanayi_tedas.psh.ToString();
            D3.Text = sanayi_kolen.psh.ToString();

            F1.Text = sanayi_tedas.kayip.ToString();
            F3.Text = sanayi_kolen.kayip.ToString();

            TRTPayi.Text = sabitler.trtpayi;
            EnerjiFonu.Text = sabitler.enerjifonu;
            BTV.Text = sabitler.btv;


        }
    }
}