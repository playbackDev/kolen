﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="indirim2.aspx.cs" Inherits="Kolen.indhesap.indirim2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="indirim-left"><!-- Ticarethane -->
	<h3 class="content-title">İndirimli Elektrik Faturası Hesaplama Tablosu</h3>
	<p>&nbsp;</p>
	<table border="0" cellpadding="0" cellspacing="0" class="table1">
		<tbody><tr>
			<td align="right">Ad Soyad</td>
			<td>:</td>
			<td colspan="2"><input type="text" name="AdSoyad" id="AdSoyad"></td>
		</tr>
		
		<tr>
			<td style="width:165px;">Aylık Ortalama Fatura Bedeli</td>
			<td>:</td>
			<td><input type="text" name="TuketimMiktari" id="TuketimMiktari" maxlength="8" onkeypress="return isNumber(event)"></td>
			<td>TL</td>
		</tr>
		
		<tr>
			<td colspan="2">&nbsp;</td>
			<td colspan="2"><a class="hes-btn" href="javascript:void(0);" onclick="Hesapla(1);" class="blue_small ir">HESAPLA</a><input type="hidden" name="IndirimOrani" id="IndirimOrani" value="8.535"></td>
		</tr>
	</tbody></table>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<table border="0" cellpadding="0" cellspacing="0" class="table1">
		<tbody><tr>
			<td class="kazanc-baslik" style="width:165px; text-align:right;">Yıllık Kazancınız</td>
			<td class="kazanc-baslik">:</td>
			<td><input type="text" name="KazancYillik" id="KazancYillik" readonly="readonly"></td>
		</tr>
	</tbody></table>
	<p >
		Not: KOLEN örnek hesaplama tablosunda değişiklik yapma hakkını saklı tutmaktadır.<br>
	</p>
</div>
<div id="indirim-right">
	<div id="fatura-ust">
		<img src="/assets/img/fatura-ust2.jpg">
		<input type="text" name="FaturaTarihi" id="FaturaTarihi" value="" readonly="readonly">
		<div id="Sayin">Sayın <input type="text" name="isim" id="isim" value="" readonly="readonly"></div>
	</div>
	<table class="fatura" cellpadding="0" cellspacing="0">
		<tbody><tr>
			<td style="text-align:left; border-right:0 none;">Abone Grubu</td>
			<td colspan="4" style="text-align:left; border-left:0 none; padding-left:0;" class="AboneGrup"><input type="text" name="AboneGrubu" id="AboneGrubu" value="Sanayi"></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2" class="baslik">TEDAŞ</td>
			<td colspan="2" class="baslik">KOLEN</td>
		</tr>
		<tr>
			<td width="109">&nbsp;</td>
			<td width="72">Birim Fiyat<br>(kr)</td>
			<td width="74" style="text-align:right;">Aylık Bedel-TL<br><span class="Aylikkwh"></span> kwh</td>
			<td width="72">Birim Fiyat<br>(kr)</td>
			<td width="74" style="text-align:right;">Aylık Bedel-TL<br><span class="Aylikkwh2"></span> kwh</td>
		</tr>
		<tr>
			<td style="text-align:left;">Per. Sat. Bed.</td>
			<td><asp:TextBox runat="server" ID="A1" Enabled="false" /> </td>
			<td><input id="A2" name="A2" value="" type="text" readonly="readonly"></td>
			<td><asp:TextBox runat="server" ID="A3" Enabled="false" /> </td>
			<td><input id="A4" name="A4" value="" type="text" readonly="readonly"></td>
		</tr>
		<tr>
			<td style="text-align:left;">Dağıtım Sis.Kull.Bed.</td>
			<td><asp:TextBox runat="server" ID="B1" Enabled="false" /> </td>
			<td><input id="B2" name="B2" value="" type="text" readonly="readonly"></td>
			<td><asp:TextBox runat="server" ID="B3" Enabled="false" /> </td>
			<td><input id="B4" name="B4" value="" type="text" readonly="readonly"></td>
		</tr>
		<tr>
			<td style="text-align:left;">İletim Bedeli</td>
			<td><asp:TextBox runat="server" ID="C1" Enabled="false" /> </td>
			<td><input id="C2" name="C2" value="" type="text" readonly="readonly"></td>
			<td><asp:TextBox runat="server" ID="C3" Enabled="false" /> </td>
			<td><input id="C4" name="C4" value="" type="text" readonly="readonly"></td>
		</tr>
		<tr>
			<td style="text-align:left;">Per. Sat. Hiz. Bed.</td>
			<td><asp:TextBox runat="server" ID="D1" Enabled="false" /> </td>
			<td><input id="D2" name="D2" value="" type="text" readonly="readonly"></td>
			<td><asp:TextBox runat="server" ID="D3" Enabled="false" /> </td>
			<td><input id="D4" name="D4" value="" type="text" readonly="readonly"></td>
		</tr>
		<tr>
			<td style="text-align:left;">Kayıp Kaçak</td>
			<td><asp:TextBox runat="server" ID="F1" Enabled="false" /> </td>
			<td><input id="F2" name="F2" value="" type="text" readonly="readonly"></td>
			<td><asp:TextBox runat="server" ID="F3" Enabled="false" /> </td>
			<td><input id="F4" name="F4" value="" type="text" readonly="readonly"></td>
		</tr>
		<tr>
			<td style="text-align:left;">Enerji Fonu %<asp:TextBox runat="server" ID="EnerjiFonu" CssClass="wth" Enabled="false" /> </td>
			<td><input id="G1" name="G1" value="" type="text" readonly="readonly"></td>
			<td><input id="G2" name="G2" value="" type="text" readonly="readonly"></td>
			<td><input id="G3" name="G3" value="" type="text" readonly="readonly"></td>
			<td><input id="G4" name="G4" value="" type="text" readonly="readonly"></td>
		</tr>
		<tr>
			<td style="text-align:left;">TRT Payı&nbsp;&nbsp;&nbsp;&nbsp;%<asp:TextBox runat="server" ID="TRTPayi" CssClass="wth" Enabled="false" /></td>
			<td><input id="H1" name="H1" value="" type="text" readonly="readonly"></td>
			<td><input id="H2" name="H2" value="" type="text" readonly="readonly"></td>
			<td><input id="H3" name="H3" value="" type="text" readonly="readonly"></td>
			<td><input id="H4" name="H4" value="" type="text" readonly="readonly"></td>
		</tr>
		<tr>
			<td style="text-align:left;">BTV&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;%<asp:TextBox runat="server" ID="BTV" CssClass="wth" Enabled="false" /></td>
			<td><input id="I1" name="I1" value="" type="text" readonly="readonly"></td>
			<td><input id="I2" name="I2" value="" type="text" readonly="readonly"></td>
			<td><input id="I3" name="I3" value="" type="text" readonly="readonly"></td>
			<td><input id="I4" name="I4" value="" type="text" readonly="readonly"></td>
		</tr>
		<tr>
			<td style="text-align:left;">Ara Toplam Fiyat</td>
			<td><input id="J1" name="J1" value="" type="text" readonly="readonly"></td>
			<td><input id="J2" name="J2" value="" type="text" readonly="readonly"></td>
			<td><input id="J3" name="J3" value="" type="text" readonly="readonly"></td>
			<td><input id="J4" name="J4" value="" type="text" readonly="readonly"></td>
		</tr>
		<tr>
			<td style="text-align:left;">K.D.V. (%18)</td>
			<td><input id="K1" name="K1" value="" type="text" readonly="readonly"></td>
			<td><input id="K2" name="K2" value="" type="text" readonly="readonly"></td>
			<td><input id="K3" name="K3" value="" type="text" readonly="readonly"></td>
			<td><input id="K4" name="K4" value="" type="text" readonly="readonly"></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td style="text-align:left;color:red;"><strong>FATURA TUTARI</strong></td>
			<td align="center"><input id="L2" name="L2" value="" type="text" readonly="readonly" class="bold" style="width:70px;"></td>
			<td align="center"><input id="L1" name="L1" value="" type="text" readonly="readonly" class="bold" style="width:70px;"></td>
			<td align="center"><input id="L3" name="L3" value="" type="text" readonly="readonly" class="bold" style="width:70px;"></td>
			<td align="center"><input id="L4" name="L4" value="" type="text" readonly="readonly" class="bold" style="width:70px;"></td>
		</tr>
	</tbody></table>
</div>
<a href="javascript:window.print();" class="faturayazdir">Yazdır</a>
     <%--  <p style="color:rgba(165, 7, 7, 0.87); font-size:20px; font-weight:bold">İndirimli Elektrik için bize ulaşın.</p> 

         <div id="genelmudurluk" class="tab-pane active">
                            <h5>ÇEPESAŞ Genel Müdürlüğü</h5>
                            <table class="table">
                                <tr>
                                    <td>Adres</td>
                                    <td>Yeşilyurt Mah. Erzincan Karayolu. 1. Km PK: 58000 Merkez / SİVAS</td>
                                </tr>
                                <tr>
                                    <td>Telefon</td>
                                    <td><strong>0 (346) 215 08 10 -15</strong></td>
                                </tr>
                                <tr>
                                    <td>Faks</td>
                                    <td><strong>0 (346) 215 08 00</strong></td>
                                </tr>
                            </table>
                        </div>--%>
    </form>
</body>
</html>
